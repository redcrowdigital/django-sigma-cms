# django-sigma-cms

This is a stand-alone module, which provides a flexible, scalable CMS
with custom node types, and flexible block content.

Authored by [Basil Shubin](http://github.com/bashu/)

## Setup

Either checkout ``django-sigmacms`` from BitBucket, or install using ``pip`` :

```bash
pip install -U --process-dependency-links git+https://bitbucket.org/sjit/django-sigma-cms.git@master#egg=django-sigamacms
```

Add ``sigmacms`` to your ``INSTALLED_APPS``.  Make sure you have ``django.contrib.sites`` installed and configured :

```python
INSTALLED_APPS = [
    ...        
    'django.contrib.contenttypes',
    'django.contrib.sites',

    'sigmacms',
]

SITE_ID = 1
```

Add the following lines to your ``settings.py`` :

```python
from sigmacms.config import setup
setup(conf=locals())
```

Update your ``urls.py`` file :

```python
from django.conf.urls.i18n import i18n_patterns

urlpatterns = patterns('',
    ...
) + i18n_patterns('',
    url(r'^', include('sigmacms.urls'))
)
```

Then run ``./manage.py syncdb`` to create the required database tables.

Please see ``example`` application. This application is used to
manually test the functionalities of this package. This also serves as
a good example.

You need Django 1.7 to run that. It might run on older versions but
that is not tested.


### External dependencies (optional, but recommended)

The following apps are optional but will enhance the user experience :

``django-suit`` modern theme for admin interface

```python
INSTALLED_APPS = [
   ...        
   'suit',  # must be before ``sigmacms``
   'sigmacms',
]

SUIT_CONFIG = {
   'ADMIN_NAME': 'Sigma CMS',
   'SEARCH_URL': '',
   'CONFIRM_UNSAVED_CHANGES': False,
   'MENU_ICONS': {
       'fluent_pages': 'icon-file',
       'filer': 'icon-picture',
   },

   'MENU': (
       {'app': 'fluent_pages', 'label': 'CMS', 'icon':'icon-file', 'models': (
           'fluent_pages.page', 'fluent_pages.pagelayout', 'sigmacms.sitesettings')},
       {'app': 'filer', 'label': 'Media Library', 'icon':'icon-picture'},
   ),
}
```

``django-post-office`` to send and manage your emails

```python
INSTALLED_APPS = [
   ...        
   'post_office',
]

if 'post_office' in INSTALLED_APPS:
   EMAIL_BACKEND = 'post_office.EmailBackend'
```

``django-authtools`` custom user model that features email as username

```python
INSTALLED_APPS = [
   ...        
   'authtools',
]

if 'authtools' in INSTALLED_APPS:
   AUTH_USER_MODEL = 'authtools.User'
```

``django-haystack`` modular search

```python
INSTALLED_APPS = [
   ...        
   'haystack',
]

if 'haystack' in INSTALLED_APPS:
   HAYSTACK_CONNECTIONS = {
       'default': {
           'ENGINE': 'haystack.backends.elasticsearch_backend.ElasticsearchSearchEngine',
           'URL': 'http://localhost:9200/',
           'INDEX_NAME': 'default',
       },
   }

   if 'LANGUAGES' in locals():
       for lang in LANGUAGES:
           HAYSTACK_CONNECTIONS.update({
               lang[0]: {
                   'ENGINE': 'haystack.backends.elasticsearch_backend.ElasticsearchSearchEngine',
                   'URL': 'http://localhost:9200/',
                   'INDEX_NAME': lang[0],
               }
           })
```

``django-export`` for adding various export options to the admin (e.g. CSV)

```python
EXTERNAL_APPS = [
    'object_tools',

    'suit',

    'export',
...
```

``social-aggregator`` for pulling data from various social sites

```python
EXTERNAL_APPS = [
    'taggit',
    'socialaggregator',
...

PROJECT_APPS = [
    'sigmacms.plugins.socialfeed',
...

## Social Aggregator settings
from socialaggregator.settings import *


EDSA_TWITTER_CONSUMER_KEY = "..."
EDSA_TWITTER_CONSUMER_SECRET = "..."
EDSA_TWITTER_TOKEN = "..."
EDSA_TWITTER_SECRET = "..."

EDSA_FB_APP_ID = "..."
EDSA_FB_APP_SECRET = "..."

EDSA_INSTAGRAM_ACCESS_TOKEN = "..."

EDSA_LINKEDIN_ACCESS_TOKEN = "..."

EDSA_PLUGINS = {
    "edsa_twitter": {
        "ENGINE": "socialaggregator.plugins.twitter_noretweet_aggregator",
        "NAME": "Twitter"
    },
    "edsa_instagram": {
        "ENGINE": "socialaggregator.plugins.instagram_aggregator",
        "NAME": "Instagram"
    },
    "edsa_facebook_fanpage": {
        "ENGINE": "socialaggregator.plugins.facebook_fanpage_aggregator",
        "NAME": "Facebook Fanpage"
    },
    "edsa_linkedin": {
        "ENGINE": "socialaggregator.plugins.linkedin_company_aggregator",
        "NAME": "LinkedIn Company"
    },
}

```

``django-analytics-tools`` for displaying analytics in the admin

```python
EXTERNAL_APPS = [
    'analytics_tools',
...

SETTINGS_DIR = os.path.join(os.path.join(BASE_DIR, 'settings'))

# Add client_secrets.json to settings directory
GOOGLE_OAUTH_CREDENTIALS = os.path.join(SETTINGS_DIR, 'client_secrets.json')
GOOGLE_ANALYTICS_PROFILE = ''

# moment.js is required, recommend using django-bower
BOWER_INSTALLED_APPS = (
    'moment#^2.13.0',
)
```

## Configuration

There are various optional configuration options you can set in your
``settings.py``, see the [django-fluent-pages documentation](https://github.com/edoburu/django-fluent-pages) for
details

