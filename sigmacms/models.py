# -*- coding: utf-8 -*-

from django.conf import settings
from django.core.exceptions import ObjectDoesNotExist

from any_urlfield.models import AnyUrlField
from any_urlfield.forms.widgets import SimpleRawIdWidget
from django.utils.text import slugify

from fluent_pages.models import (
    UrlNode, UrlNode_Translation, PageLayout, Page, HtmlPage)
from fluent_pages.integration.fluent_contents.models import FluentContentsPage
from fluent_pages import appsettings


if 'publisher' in settings.INSTALLED_APPS:

    class SigmaPageMixin(object):

        publisher_clone_fields = [
            'publication_date',
            'publication_end_date',
            'in_navigation',
            'in_sitemaps',
        ]

        class Meta:
            abstract = True
            permissions = (
                ('can_publish', 'Can publish'),
            )

        def save(self, suppress_modified=False, **kwargs):
            if suppress_modified is False:
                self.update_modified_at()

            super(SigmaPageMixin, self).save(**kwargs)

        @staticmethod
        def clone_translations(src_obj, dst_obj):
            super(SigmaPageMixin, src_obj).clone_translations(src_obj, dst_obj)

            if hasattr(src_obj, 'seo_translations'):
                for t in src_obj.seo_translations.all():
                    language_code = t.language_code
                    try:
                        translation = src_obj.publisher_linked.seo_translations.get(
                            language_code=language_code)
                    except (ObjectDoesNotExist, AttributeError):
                        translation = t
                        translation.pk = None
                        translation.master = dst_obj

                    for field, value in t.__dict__.iteritems():
                        if field not in ['id', 'master_id', 'language_code']:
                            setattr(translation, field, value)
                    translation.save()

        def delete(self):
            if hasattr(self, 'seo_translations'):
                self.seo_translations.all().delete()

            if self.is_draft is True:
                for p in self.get_children().filter(publisher_is_draft=self.STATE_DRAFT):
                    p.delete()

            super(SigmaPageMixin, self).delete()

        @staticmethod
        def copy_translations(rc_obj, dst_obj):
            pass

        def clone_page(self):
            self.save(suppress_modified=True)

            # get parent object
            try:
                parent = UrlNode.objects.non_polymorphic().get(children__pk=self.id)
            except UrlNode.DoesNotExist:
                parent = None

            new_obj = self.__class__.objects.get(pk=self.pk)
            for fld in ('pk', 'id', 'publisher_linked_id'):
                setattr(new_obj, fld, None)
            new_obj.published = False

            # setting the tree
            if not parent:
                new_obj.level = 0
                new_obj.tree_id = None
            new_obj.lft = None
            new_obj.rght = None
            new_obj.save()

            # copy translations
            if hasattr(self, 'translations'):
                all_translations = UrlNode_Translation.objects.all()
                if appsettings.FLUENT_PAGES_FILTER_SITE_ID:
                    site_id = self.parent_site_id or settings.SITE_ID
                    all_translations = all_translations.filter(master__parent_site=site_id)

                for translation in self.translations.all():
                    translation.pk = None

                    # unique title
                    temp_title = new_title = "%s (Copy)" % translation.title
                    number = 1
                    while all_translations.filter(title=new_title).count():
                        new_title = "%s %d" % (temp_title, number)
                        number += 1
                    translation.title = new_title

                    # unique slug
                    temp_slug = new_slug = slugify(translation.title).replace('-', '_')
                    number = 1
                    while all_translations.filter(slug=new_slug).count():
                        new_slug = "%s_%d" % (temp_slug, number)
                        number += 1

                    translation.slug = new_slug

                    # unique _cached_url
                    if parent:
                        new_url = '%s%s/' % (parent._cached_url, new_slug)
                    else:
                        new_url = '/%s/' % new_slug

                    number = 1
                    while all_translations.filter(_cached_url=new_url).count():
                        if parent:
                            new_url = '%s%s_%d/' % (parent._cached_url, new_slug, number)
                        else:
                            new_url = '/%s_%d/' % (new_slug, number)
                        number += 1
                    translation._cached_url = new_url
                    translation.master = new_obj
                    translation.save()

            draft_obj = self
            self.copy_translations(draft_obj, new_obj)
            self.clone_placeholder(draft_obj, new_obj)
            self.clone_relations(draft_obj, new_obj)


    from fluent_pages.forms.fields import PageChoiceField

    for index, t in enumerate(AnyUrlField._static_registry._url_types):
        if t.model == Page:
            del AnyUrlField._static_registry._url_types[index]

    AnyUrlField.register_model(
        Page, form_field=lambda: PageChoiceField(
            queryset=Page.objects.filter(publisher_is_draft=True, published=True),
            widget=SimpleRawIdWidget(Page, limit_choices_to={
                'publisher_is_draft': True, 'published': True})))

else:

    class SigmaPageMixin(object):
        pass
