# -*- coding: utf-8 -*-

import json
import django

from django.conf import settings
from django.db import transaction
from django.http import (
    HttpResponseNotFound, HttpResponse, HttpResponseBadRequest, HttpResponseRedirect)
from django.contrib.admin.templatetags.admin_urls import add_preserved_filters
from django.utils.translation import ugettext_lazy as _
from django.core.urlresolvers import reverse

from parler.utils import is_multilingual_project
from parler.templatetags.parler_tags import get_translated_url
from fluent_pages import appsettings

try:
    transaction_atomic = transaction.atomic
except AttributeError:
    transaction_atomic = transaction.commit_on_success

if 'sigmacms.contrib.fluent_suit' in settings.INSTALLED_APPS:
    from sigmacms.contrib.fluent_suit.admin import SuitAdminMixin
else:
    class SuitAdminMixin(object):
        pass


if 'publisher' in settings.INSTALLED_APPS:
    from publisher.admin import PublisherAdmin as PublisherAdminBase
    from publisher.admin import PublisherForm

    from fluent_pages.adminui.urlnodechildadmin import UrlNodeAdminForm

    class PageAdminForm(UrlNodeAdminForm, PublisherForm):

        def clean(self):
            from sigmacms.models import UrlNode, UrlNode_Translation

            cleaned_data = super(UrlNodeAdminForm, self).clean()

            all_nodes = UrlNode.objects.all()
            all_translations = UrlNode_Translation.objects.all()
            if appsettings.FLUENT_PAGES_FILTER_SITE_ID:
                site_id = (self.instance is not None and self.instance.parent_site_id) or settings.SITE_ID
                all_nodes = all_nodes.filter(parent_site=site_id)
                all_translations = all_translations.filter(master__parent_site=site_id)

            if self.instance and self.instance.id:
                # Editing an existing page
                obj, current_id = self.instance, self.instance.id
                other_nodes = all_nodes.exclude(
                    id__in=[current_id, obj.publisher_linked_id])
                other_translations = all_translations.exclude(
                    master_id__in=[current_id, obj.publisher_linked_id])

                # Get original unmodified parent value.
                try:
                    parent = UrlNode.objects.non_polymorphic().get(children__pk=current_id)
                except UrlNode.DoesNotExist:
                    parent = None
            else:
                # Creating new page!
                parent = cleaned_data['parent']
                other_nodes = all_nodes
                other_translations = all_translations

            # Unique check for the `key` field.
            if cleaned_data.get('key'):
                if other_nodes.filter(key=cleaned_data['key']).count():
                    self._errors['key'] = self.error_class([
                        _('This identifier is already used by an other page.')])
                    del cleaned_data['key']

            # If fields are filled in, and still valid, check for unique URL.
            # Determine new URL (note: also done in UrlNode model..)
            if cleaned_data.get('override_url'):
                new_url = cleaned_data['override_url']

                if other_translations.filter(_cached_url=new_url).count():
                    self._errors['override_url'] = self.error_class([
                        _('This URL is already taken by an other page.')])
                    del cleaned_data['override_url']

            elif cleaned_data.get('slug'):
                new_slug = cleaned_data['slug']
                if parent:
                    new_url = '%s%s/' % (parent._cached_url, new_slug)
                else:
                    new_url = '/%s/' % new_slug

                if other_translations.filter(_cached_url=new_url).count():
                    self._errors['slug'] = self.error_class([
                        _('This slug is already used by an other page at the same level.')])
                    del cleaned_data['slug']

            return cleaned_data

    
    class PublisherAdmin(PublisherAdminBase):

        def has_publish_permission(self, request, obj=None):
            user_obj = request.user

            if user_obj.is_active and user_obj.is_publisher:
                return True

            return False

        def queryset(self, request):
            # hack! We need request.user to check user publish perms
            self.request = request
            qs = self.model.objects
            qs_language = self.get_queryset_language(request)
            if qs_language:
                qs = qs.language(qs_language)
            qs = qs.filter(publisher_is_draft=True)
            ordering = getattr(self, 'ordering', None) or ()
            if ordering:
                qs = qs.order_by(*ordering)
            return qs

        def response_change(self, request, obj):
            opts = self.model._meta
            pk_value = obj._get_pk_val()
            preserved_filters = self.get_preserved_filters(request)

            if "_publish" in request.POST:
                redirect_url = reverse(self.publish_reverse, args=[pk_value], current_app=self.admin_site.name)
                redirect_url = add_preserved_filters({
                    'preserved_filters': preserved_filters, 'opts': opts}, redirect_url)
                return HttpResponseRedirect(redirect_url)

            return super(PublisherAdmin, self).response_change(request, obj)

        def render_change_form(self, request, context, add=False, change=False, form_url='', obj=None):
            obj = context.get('original', None)
            if not obj:
                return super(PublisherAdminBase, self).render_change_form(
                    request, context, add, change, form_url, obj=None)

            preview_draft_btn = None
            if callable(getattr(obj, 'get_absolute_url', None)):
                preview_draft_btn = True

            if not self.has_publish_permission(request, obj):
                context['has_publish_permission'] = False
            else:
                context['has_publish_permission'] = True

                publish_btn = None
                if obj.is_dirty:
                    publish_btn = reverse(self.publish_reverse, args=(obj.pk, ))

                unpublish_btn = None
                if obj.is_draft and obj.publisher_linked and obj.published:
                    unpublish_btn = reverse(self.unpublish_reverse, args=(obj.pk, ))

                revert_btn = None
                if obj.is_dirty and obj.publisher_linked and obj.published:
                    revert_btn = reverse(self.revert_reverse, args=(obj.pk, ))

                context.update({
                    'published_url': get_translated_url(
                        dict(request=request),
                        self.get_form_language(request, obj),
                        obj.publisher_linked,
                    ),
                    'publish_btn': publish_btn,
                    'unpublish_btn': unpublish_btn,
                    'revert_btn': revert_btn,
                })

            context.update({
                'preview_draft_btn': preview_draft_btn,
            })

            return super(PublisherAdminBase, self).render_change_form(
                request, context, add=add, change=change, form_url=form_url, obj=obj)


    class SigmaAdminMixin(SuitAdminMixin, PublisherAdmin):
        from parler import appsettings as parler_appsettings

        actions = None

        if is_multilingual_project() and len(parler_appsettings.PARLER_LANGUAGES.get(settings.SITE_ID)) > 1:
            list_display = (
                'publisher_object_title', 'language_column', 'status_column', 'pagetype_column', 'modification_date', 'actions_column')
        else:
            list_display = (
                'publisher_object_title', 'status_column', 'pagetype_column', 'modification_date', 'actions_column')
        list_filter = []

        FIELDSET_GENERAL = (None, {
            'fields': ('title', 'slug', 'in_navigation'),
        })
        FIELDSET_MENU = (_('Menu structure'), {
            'fields': ('parent',),
            'classes': ('collapse',),
        })
        #: The publication fields.
        FIELDSET_PUBLICATION = (_('Publication settings'), {
            'fields': ('publication_date', 'publication_end_date', 'override_url',),
            'classes': ('collapse',),
        })
        if appsettings.FLUENT_PAGES_KEY_CHOICES:
            FIELDSET_PUBLICATION[1]['fields'] += ('key',)

        base_fieldsets = (
            FIELDSET_GENERAL,
            FIELDSET_MENU,
            FIELDSET_PUBLICATION,
        )

        if not appsettings.FLUENT_PAGES_KEY_CHOICES:
            exclude = ('key', 'status')
        else:
            exclude = ('status',)

        def status_column(self, urlnode):
            if urlnode.publisher_linked and urlnode.published:
                if urlnode.is_dirty is True:
                    return _("DRAFT + PUBLISHED")
                if urlnode.is_dirty is False:
                    return _("PUBLISHED")
            return _("DRAFT")
        status_column.short_description = _('Status')

        def pagetype_column(self, urlnode):
            return urlnode._meta.verbose_name
        pagetype_column.short_description = _('Type')

        @transaction_atomic
        def api_node_moved_view(self, request):
            try:
                moved_id = int(request.POST['moved_id'])
                target_id = int(request.POST['target_id'])
                position = request.POST['position']
                previous_parent_id = int(request.POST['previous_parent_id']) or None

                # Not using .non_polymorphic() so all models are downcasted to the derived model.
                # This causes the signal below to be emitted from the proper class as well.
                moved = self.model.objects.get(pk=moved_id)
                target = self.model.objects.get(pk=target_id)
            except (ValueError, KeyError) as e:
                return HttpResponseBadRequest(json.dumps({
                    'action': 'foundbug', 'error': str(e[0])}), content_type='application/json')
            except self.model.DoesNotExist as e:
                return HttpResponseNotFound(json.dumps({
                    'action': 'reload', 'error': str(e[0])}), content_type='application/json')

            if position == 'inside':
                if not self.can_have_children(target):
                    return HttpResponse(json.dumps({
                        'action': 'reject',
                        'moved_id': moved_id,
                        'error': _(u'Cannot place \u2018{0}\u2019 below \u2018{1}\u2019; a {2} does not allow children!').format(moved, target, target._meta.verbose_name),
                    }), content_type='application/json', status=409)  # Conflict
                elif not self.can_have_children(target, moved):
                    return HttpResponse(json.dumps({
                        'action': 'reject',
                        'moved_id': moved_id,
                        'error': _(u"Cannot place \u2018{0}\u2019 below \u2018{1}\u2019; a {2} does not allow {3} as a child!").format(moved, target, target._meta.verbose_name, moved._meta.verbose_name),
                    }), content_type='application/json', status=409)  # Conflict
            if moved.parent_id != previous_parent_id:
                return HttpResponse(json.dumps({
                    'action': 'reload',
                    'error': 'Client seems to be out-of-sync, please reload!'
                }), content_type='application/json', status=409)

            # TODO: with granular user permissions, check if user is allowed to edit both pages.

            mptt_position = {
                'inside': 'first-child',
                'before': 'left',
                'after': 'right',
            }[position]
            moved.move_to(target, mptt_position)

            if moved.is_draft and moved.publisher_linked:
                if target.is_draft and target.publisher_linked:
                    moved.publisher_linked.move_to(target.publisher_linked, mptt_position)

            # Some packages depend on calling .save() or post_save signal after updating a model.
            # This is required by django-fluent-pages for example to update the URL caches.
            moved.save()

            if moved.is_draft and moved.publisher_linked:
                if target.is_draft and target.publisher_linked:
                    moved.publisher_linked.save()

            # Report back to client.
            return HttpResponse(json.dumps({
                'action': 'success',
                'error': None,
                'moved_id': moved_id,
                'action_column': self.actions_column(moved),
            }), content_type='application/json')

        def get_translation_objects(self, request, language_code, obj=None, inlines=True):
            if obj is not None:
                for translations_model in obj._parler_meta.get_all_models():
                    try:
                        translation = translations_model.objects.get(master=obj, language_code=language_code)
                    except translations_model.DoesNotExist:
                        continue
                    yield [translation]

                if obj.is_draft and obj.publisher_linked:
                    for translations_model in obj.publisher_linked._parler_meta.get_all_models():
                        try:
                            translation = translations_model.objects.get(
                                master=obj.publisher_linked, language_code=language_code)
                        except translations_model.DoesNotExist:
                            continue
                        yield [translation]

            if inlines:
                for inline, qs in self._get_inline_translations(request, language_code, obj=obj):
                    yield qs

        def get_action_icons(self, node):
            """
            Return a list of all action icons in the :func:`actions_column`.
            """
            actions = []
            if self.can_have_children(node):
                actions.append(
                    u'<a href="add/?{parent_attr}={id}" title="{title}" class="add-child-object"><i class="icon-plus-sign icon-alpha75"></i></a>'.format(
                        parent_attr=self.model._mptt_meta.parent_attr, id=node.pk, title=_('Add sub node'),
                        static=settings.STATIC_URL)
                )
            else:
                actions.append(
                    self.EMPTY_ACTION_ICON.format(STATIC_URL=settings.STATIC_URL, css_class='add-child-object'))

            if self.has_add_permission(self.request):
                actions.append(
                    u'<a href="{url}" title="{title}" target="_self" class="clone-page"><i class="icon-clipboard icon-alpha75"></i></a>'.format(
                        url='%s/clone/' % node.pk, title=_('Clone page'), static=settings.STATIC_URL)
                )

            if hasattr(node, 'published'):
                if node.published:
                    temp_url = get_translated_url(
                            dict(request=self.request),
                            self.get_form_language(self.request, node),
                            node.publisher_linked,
                        )
                    actions.append(
                        u'<a href="{url}" title="{title}" target="_blank" class="viewsitelink"><i class="icon-globe icon-alpha75"></i></a>'.format(
                            url=temp_url, title=_('View on site'), static=settings.STATIC_URL)
                    )
                actions.append(
                    u'<a href="{url}" title="{title}" target="_blank" class="previewdraftlink"><i class="icon-eye-open icon-alpha75"></i></a>'.format(
                        url='%s?edit' % node.url, title=_('Preview draft'), static=settings.STATIC_URL)
                )

            # The is_first_sibling and is_last_sibling is quite heavy. Instead rely on CSS to hide the arrows.
            move_up = u'<a href="{0}/move_up/" class="move-up">\u2191</a>'.format(node.pk)
            move_down = u'<a href="{0}/move_down/" class="move-down">\u2193</a>'.format(node.pk)
            actions.append(u'<span class="no-js">{0}{1}</span>'.format(move_up, move_down))
            return actions


else:
    from fluent_pages.adminui.urlnodechildadmin import UrlNodeAdminForm

    class PageAdminForm(UrlNodeAdminForm):
        pass
    
    class SigmaAdminMixin(SuitAdminMixin):
        actions = None
