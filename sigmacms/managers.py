# -*- coding: utf-8 -*-

from django.db import models
from django.conf import settings
from django.utils.timezone import now
from django.db.models.query_utils import Q
from django.core.exceptions import ObjectDoesNotExist

from model_utils.managers import PassThroughManagerMixin
from fluent_pages.models.managers import UrlNodeQuerySet, UrlNodeManager
from fluent_pages import appsettings


if 'publisher' in settings.INSTALLED_APPS:
    from publisher.managers import PublisherQuerySet
    from publisher.middleware import get_draft_status
    from publisher.utils import assert_draft
    from publisher.signals import (
        publisher_publish_pre_save_draft,
        publisher_pre_publish,
        publisher_post_publish,
        publisher_pre_unpublish,
        publisher_post_unpublish,
    )
    from .signals import publisher_pre_delete

    class UrlNodeQuerySet(UrlNodeQuerySet):

        # override ``published`` method...
        def published(self, for_user=None):
            from fluent_pages.models import UrlNode
            from publisher.models import PublisherModelBase

            qs = self.filter(publisher_is_draft=get_draft_status())

            if not get_draft_status():
                qs = qs.filter(published=True)

            if for_user is not None and for_user.is_staff:
                return qs._single_site()

            return qs._single_site().filter(
                Q(publication_date__isnull=True) | Q(publication_date__lt=now())
            ).filter(
                Q(publication_end_date__isnull=True) | Q(publication_end_date__gte=now())
            )

        
    class UrlNodeManager(UrlNodeManager):
        queryset_class = UrlNodeQuerySet

        
    class PublisherQuerySet(PublisherQuerySet, UrlNodeQuerySet):
        pass

    
    class PublisherManager(PassThroughManagerMixin, UrlNodeManager):

        def contribute_to_class(self, model, name):
            super(PublisherManager, self).contribute_to_class(model, name)

            from publisher.models import PublisherModelBase as PublisherModel

            attributes = [
                'STATE_DRAFT',
                'STATE_PUBLISHED',

                'publisher_fields',
                'publisher_ignore_fields',
                'publisher_publish_empty_fields',

                'is_draft',
                'is_published',
            ]

            for attr in attributes:
                model.add_to_class(attr, getattr(PublisherModel, attr))

            methods = [
                'get_placeholder_fields',
                'get_unique_together',
                'get_field',
            ]

            for attr in methods:
                setattr(model, attr, getattr(
                    PublisherModel, attr).__get__(model, PublisherModel))

            def is_dirty(cls):
                if not cls.is_draft:
                    return False

                # If the record has not been published assume dirty
                if not cls.publisher_linked:
                    return True

                if cls.publisher_linked and not cls.published:
                    return True

                if cls.publisher_modified_at > cls.publisher_linked.publisher_modified_at:
                    return True

                # Get all placeholders + their plugins to find their modified date
                for placeholder_field in cls.get_placeholder_fields():
                    placeholder = getattr(cls, placeholder_field)
                    for plugin in placeholder.get_plugins_list():
                        if plugin.changed_date > cls.publisher_linked.publisher_modified_at:
                            return True

                return False

            model.add_to_class('is_dirty', property(is_dirty))

            @assert_draft
            def publish(cls):
                if not cls.is_draft:
                    return

                if not cls.is_dirty:
                    return

                publisher_pre_publish.send(sender=cls.__class__, instance=cls)

                draft_obj = cls

                # Set the published date if this is the
                # first time the page has been published
                if not draft_obj.publisher_linked:
                    draft_obj.publisher_published_at = now()

                if draft_obj.publisher_linked:
                    cls.patch_placeholders(draft_obj)

                    publish_obj = draft_obj.publisher_linked

                else:
                    publish_obj = cls.__class__.objects.get(pk=cls.pk)

                    for fld in cls.publisher_publish_empty_fields:
                        setattr(publish_obj, fld, None)

                publish_obj.publisher_is_draft = cls.STATE_PUBLISHED
                publish_obj.publisher_published_at = draft_obj.publisher_published_at

                publish_obj.published = True
                draft_obj.published = True

                publish_obj.save()

                cls.clone_translations(draft_obj, publish_obj)
                cls.clone_placeholder(draft_obj, publish_obj)
                cls.clone_relations(draft_obj, publish_obj)
                cls.clone_fields(draft_obj, publish_obj)

                # Link the draft obj to the current published version
                draft_obj.publisher_linked = publish_obj

                # Clone mptt structure
                cls.clone_mptt_structure(draft_obj, publish_obj)

                # rebuild mptt tree...
                cls.rebuild_tree()

                publisher_publish_pre_save_draft.send(
                    sender=draft_obj.__class__, instance=draft_obj)

                draft_obj.save(suppress_modified=True)
                
                publisher_post_publish.send(
                    sender=draft_obj.__class__, instance=draft_obj)

            model.publish = publish
            
            @assert_draft
            def patch_placeholders(cls, draft_obj):
                try:
                    from fluent_contents.models import Placeholder
                except ImportError:
                    return

                published_obj = draft_obj.publisher_linked

                for draft_placeholder, published_placeholder in zip(
                        Placeholder.objects.parent(draft_obj), Placeholder.objects.parent(published_obj)):

                    if draft_placeholder.pk == published_placeholder.pk:
                        published_placeholder.pk = None
                        published_placeholder.save()

            model.patch_placeholders = patch_placeholders

            @assert_draft
            def unpublish(cls):
                if not cls.is_draft or not cls.publisher_linked:
                    return

                publisher_pre_unpublish.send(sender=cls.__class__, instance=cls) 
                cls.publisher_linked.published = cls.published = False
                cls.publisher_linked.save(update_fields=['published'])
                cls.save(update_fields=['published'])
                publisher_post_unpublish.send(sender=cls.__class__, instance=cls)

            model.unpublish = unpublish

            @assert_draft
            def revert_to_public(cls):
                if not cls.publisher_linked:
                    return

                # Get published obj and delete the draft
                draft_obj, publish_obj = cls, cls.publisher_linked

                draft_obj.publisher_linked = None
                draft_obj.save()

                draft_obj.delete()

                # Mark the published object as a draft
                draft_obj, publish_obj = publish_obj, None

                draft_obj.publisher_is_draft = draft_obj.STATE_DRAFT
                draft_obj.save()
                draft_obj.publish()

                return draft_obj

            model.revert_to_public = revert_to_public

            @staticmethod
            def clone_translations(src_obj, dst_obj):
                if hasattr(src_obj, 'translations'):
                    for t in src_obj.translations.all():
                        language_code = t.language_code
                        try:
                            translation = src_obj.publisher_linked.translations.get(
                                language_code=language_code)
                        except (ObjectDoesNotExist, AttributeError):
                            translation = t
                            translation.pk = None
                            translation.master = dst_obj

                        for field, value in t.__dict__.iteritems():
                            if field not in ['id', 'master_id', 'language_code']:
                                setattr(translation, field, value)
                        translation.save()

            model.clone_translations = clone_translations

            def clone_placeholder(cls, src_obj, dst_obj):
                try:
                    from fluent_contents.models import Placeholder, ContentItem
                except ImportError:
                    return

                for c in ContentItem.objects.parent(dst_obj, False):
                    c.delete()
                Placeholder.objects.parent(dst_obj).delete()

                for src_placeholder in Placeholder.objects.parent(src_obj):
                    dst_placeholder = Placeholder.objects.create_for_object(
                        dst_obj, slot=src_placeholder.slot, role=src_placeholder.role, title=src_placeholder.title)

                    src_items = src_placeholder.get_content_items()

                    src_items.copy_to_placeholder(dst_placeholder)

            model.clone_placeholder = clone_placeholder

            def clone_relations(cls, src_obj, dst_obj):
                pass

            model.clone_relations = clone_relations

            def clone_fields(cls, src_obj, dst_obj):
                for field in getattr(cls, 'publisher_clone_fields', []):
                    value = getattr(src_obj, field, None)
                    if value != getattr(dst_obj, field, None):
                        setattr(dst_obj, field, value)

                dst_obj.save(suppress_modified=True, update_fields=getattr(
                    cls, 'publisher_clone_fields', []))

            model.clone_fields = clone_fields

            def clone_mptt_structure(cls, src_obj, dst_obj):
                if src_obj.publisher_linked:
                    if src_obj.is_root_node():
                        parent = None
                    else:
                        parent = src_obj.parent.publisher_linked
                    if parent != cls.publisher_linked.parent:
                        dst_obj.parent = parent
                dst_obj.save()

            model.clone_mptt_structure = clone_mptt_structure
            
            def rebuild_tree(cls):
                if hasattr(cls.__class__, 'rebuild'):
                    cls.__class__.rebuild()
                else:
                    cls.__class__.objects.rebuild()

            model.rebuild_tree = rebuild_tree

            def update_modified_at(cls):
                cls.publisher_modified_at = now()
        
            model.update_modified_at = update_modified_at

            # override ``_make_slug_unique`` method...
            def _make_slug_unique(cls, translation):
                from fluent_pages.models import UrlNode

                origslug = translation.slug
                dupnr = 1
                while True:
                    others = UrlNode.objects.filter(
                        parent=cls.parent_id,
                        translations__slug=translation.slug,
                        translations__language_code=translation.language_code
                    ).non_polymorphic()

                    if appsettings.FLUENT_PAGES_FILTER_SITE_ID:
                        others = others.parent_site(cls.parent_site_id)

                    if cls.pk:
                        others = others.exclude(pk__in=[cls.pk, cls.publisher_linked_id])

                    if not others.count():
                        break

                    dupnr += 1
                    translation.slug = "%s-%d" % (origslug, dupnr)

            model._make_slug_unique = _make_slug_unique

            models.signals.pre_delete.connect(publisher_pre_delete, model)
