# -*- coding: utf-8 -*-

from __future__ import unicode_literals

from collections import defaultdict

from django.contrib import admin
from django.utils.functional import curry
from django.db.models import OneToOneField
from django.core.exceptions import ImproperlyConfigured
from django.db.models.signals import class_prepared

from django.conf import settings

from .lazy_admin import LazyAdminSite
from .utils import import_dotted_path


# Convert ``EXTRA_MODEL_FIELDS`` into a more usable structure, a
# dictionary mapping module.model paths to dicts of field names mapped
# to field instances to inject, with some sanity checking to ensure
# the field is importable and the arguments given for it are valid.
def add_extra_model_fields(sender, **kwargs):
    """
    Injects custom fields onto the given sender model as defined
    by the ``EXTRA_MODEL_FIELDS`` setting.

    """
    for entry in getattr(settings, "EXTRA_MODEL_FIELDS", []):
        model_path, field_name = entry[0].rsplit(".", 1)
        field_path, field_args, field_kwargs = entry[1:]

        if not model_path == "%s.%s" % (sender.__module__, sender.__name__):
            continue

        if "." not in field_path:
            field_path = "django.db.models.%s" % field_path
        try:
            field_class = import_dotted_path(field_path)
        except ImportError:
            raise ImproperlyConfigured("The EXTRA_MODEL_FIELDS setting contains "
                                       "the field '%s' which could not be "
                                       "imported." % entry[1])
        try:
            # handle relation to self...
            if issubclass(field_class, OneToOneField):
                if 'to' in field_kwargs.keys():
                    del field_kwargs['to']

                if ('self' in field_args) or (model_path in field_args):
                    field_args = list(field_args)
                    if (field_args[0] == 'self') or (field_args[0] == model_path):
                        field_args[0] = sender

            field = field_class(*field_args, **field_kwargs)
        except TypeError as e:
            raise ImproperlyConfigured("The EXTRA_MODEL_FIELDS setting contains "
                                       "arguments for the field '%s' which could "
                                       "not be applied: %s" % (entry[1], e))

        field.contribute_to_class(sender, field_name)

class_prepared.connect(add_extra_model_fields, dispatch_uid="FQFEQ#rfq3r")

# Override django.contrib.admin.site with LazyAdminSite. It must
# be bound to a separate name (admin_site) for access in autodiscover
# below.

admin_site = LazyAdminSite()
admin.site = admin_site
django_autodiscover = admin.autodiscover


def autodiscover(*args, **kwargs):
    """
    Replaces django's original autodiscover to add a call to
    LazyAdminSite's lazy_registration.

    """
    django_autodiscover(*args, **kwargs)
    admin_site.lazy_registration()

admin.autodiscover = autodiscover
