# -*- coding: utf-8 -*-

from __future__ import unicode_literals

from django.apps import apps
from django.conf import settings
from django.http.response import Http404
from django.core.urlresolvers import reverse, NoReverseMatch
from django.template.response import TemplateResponse
from django.core.exceptions import PermissionDenied
from django.utils.translation import ugettext as _
from django.http import HttpResponseRedirect
from django.utils.text import capfirst

from axes.decorators import watch_login

if 'adminplus' in settings.INSTALLED_APPS:
    from adminplus.sites import AdminSitePlus as AdminSite
else:
    from django.contrib.admin.sites import AdminSite


class LazyAdminSite(AdminSite):
    """
    Defers calls to register/unregister until autodiscover is called
    to avoid load issues with injectable model fields defined by
    ``settings.EXTRA_MODEL_FIELDS``.

    """
    def __init__(self, *args, **kwargs):
        self._deferred = []
        super(LazyAdminSite, self).__init__(*args, **kwargs)

    def register(self, *args, **kwargs):
        for name, deferred_args, deferred_kwargs in self._deferred:
            if name == "unregister" and deferred_args[0] == args[0]:
                self._deferred.append(("register", args, kwargs))
                break
        else:
            super(LazyAdminSite, self).register(*args, **kwargs)

    def unregister(self, *args, **kwargs):
        self._deferred.append(("unregister", args, kwargs))

    def lazy_registration(self):
        for name, deferred_args, deferred_kwargs in self._deferred:
            getattr(AdminSite, name)(self, *deferred_args, **deferred_kwargs)

    def login(self, request, extra_context=None):
        func = super(LazyAdminSite, self).login
        return watch_login(func)(request, extra_context)

    def app_index(self, request, app_label, extra_context=None):
        if request.user.is_superuser is True:
            return super(LazyAdminSite, self).app_index(request, app_label, extra_context)

        app_name = apps.get_app_config(app_label).verbose_name

        if not request.user.has_module_perms(app_label):
            raise PermissionDenied

        if not request.user.is_superuser and request.user.is_editor:
            return HttpResponseRedirect(reverse('admin:index', current_app=self.name))

        app_dict = {}

        for model, model_admin in getattr(self, 'registry', {}).items():
            if app_label != model._meta.app_label:
                continue

            perms = model_admin.get_model_perms(request)
            # Check whether user has any perm for this module.
            # If so, add the module to the model_list.
            if True not in perms.values():
                continue

            info = (app_label, model._meta.model_name)
            model_dict = {
                'name': capfirst(model._meta.verbose_name_plural),
                'object_name': model._meta.object_name,
                'perms': perms,
            }
            if perms.get('change'):
                try:
                    model_dict['admin_url'] = reverse('admin:%s_%s_changelist' % info, current_app=self.name)
                except NoReverseMatch:
                    pass
            if perms.get('add'):
                try:
                    model_dict['add_url'] = reverse('admin:%s_%s_add' % info, current_app=self.name)
                except NoReverseMatch:
                    pass

            if app_dict:
                app_dict['models'].append(model_dict)
            else:
                app_dict = {
                    'name': app_name,
                    'app_label': app_label,
                    'app_url': '',
                    'has_module_perms': has_module_perms,
                    'models': [model_dict],
                }

        if not app_dict:
            raise Http404('The requested admin page does not exist.')
        # Sort the models alphabetically within each app.
        app_dict['models'].sort(key=lambda x: x['name'])
        context = dict(
            self.each_context(),
            title=_('%(app)s administration') % {'app': app_name},
            app_list=[app_dict],
            app_label=app_label,
        )
        context.update(extra_context or {})

        request.current_app = self.name

        return TemplateResponse(request, self.app_index_template or [
            'admin/%s/app_index.html' % app_label,
            'admin/app_index.html'
        ], context)
