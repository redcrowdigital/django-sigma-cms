# -*- coding: utf-8 -*-

from django.views.generic import RedirectView
from django.contrib.staticfiles.storage import staticfiles_storage

from fluent_pages.views.dispatcher import CmsPageDispatcher


class FaviconView(RedirectView):
    permanent=True

    def get_redirect_url(self, *args, **kwargs):
        self.url = staticfiles_storage.url('favicon.ico')

        from usersettings.shortcuts import get_current_usersettings
        from easy_thumbnails.files import get_thumbnailer

        usersetting = get_current_usersettings()
        if getattr(usersetting, 'favicon', None) is not None:
            try:
                t = get_thumbnailer(usersetting.favicon).get_thumbnail(
                    {'size': (16, 16), 'crop': True})
                self.url = t.url
            except:
                pass
        return super(FaviconView, self).get_redirect_url(*args, **kwargs)

        
class CmsPageDispatcher(CmsPageDispatcher):

    def get(self, request, **kwargs):
        self.language_code = self.get_language()
        self.path = self.get_path()

        # See which view returns a valid response.
        for func in (
            self._try_node,
            self._try_node_redirect,
            self._try_appnode,
            self._try_append_slash_redirect,
            self._try_homepage
        ):
            response = func()
            if response is not None:
                return response

        return self._page_not_found()
    
    def _try_homepage(self):
        if not self.path == '/':
            return None

        if not self.get_queryset().toplevel().exists():
            return None

        try:
            # getting first toplevel page
            self.object = self.get_queryset().toplevel()[0]
        except IndexError:
            return None

        return self._call_node_view(self.get_plugin())
