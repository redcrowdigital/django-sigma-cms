from django.conf import settings
from django.conf.urls import include, patterns, url

from fluent_pages.views import CmsPageAdminRedirect


if 'adminplus' in settings.INSTALLED_APPS:
    from sigmacms.bootstrap import admin
    admin.autodiscover()

from sigmacms import admin as dashboard
from .views import CmsPageDispatcher, FaviconView

urlpatterns = [
    url(r'^dashboard/', include(dashboard.site.urls)),
    url(r'^favicon\.ico$', FaviconView.as_view(), name='favicon'),
]

urlpatterns += url(r'^dashboard/', include('loginas.urls')),

if 'analytics_tools' in settings.INSTALLED_APPS:
    urlpatterns += url(r'^dashboard/', include('analytics_tools.urls')),

if 'object_tools' in settings.INSTALLED_APPS:
    import object_tools
    urlpatterns += [
        url(r'^dashboard/object-tools/', include(object_tools.tools.urls)),
    ]

if 'sigmacms.contrib.simplesearch' in settings.INSTALLED_APPS:
    urlpatterns += [
        url(r'^search/', include('sigmacms.contrib.simplesearch.urls')),
    ]

urlpatterns += patterns('fluent_pages.views',
    url(r'^(?P<path>.*)@admin$', CmsPageAdminRedirect.as_view(), name='fluent-page-admin-redirect'),
    url(r'^(?P<path>.*)$', CmsPageDispatcher.as_view(), name='fluent-page-url'),
    url(r'^$', CmsPageDispatcher.as_view(), name='fluent-page'),
)
