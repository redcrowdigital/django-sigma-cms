from authtools.models import User
from django.contrib.auth import authenticate
from django.core.urlresolvers import resolve
from django.test import TestCase, RequestFactory


class UserTestCase(TestCase):
    def setUp(self):
        self.admin = User.objects.create_superuser(email='admin@sigma.com', password='111111')
        self.site_user = User.objects.create_user(email='user@sigma.com', password='111111')
        self.publisher = User.objects.create_user(email='publisher@sigma.com', password='111111')
        self.editor = User.objects.create_user(email='editor@sigma.com', password='111111')
        self.factory = RequestFactory()

    def test_can_login(self):
        self.assertTrue(authenticate(username='admin@sigma.com', password='111111'))

    def test_site_user_is_not_staff(self):
        self.assertFalse(self.site_user.is_staff)

    def test_publisher_has_editor_perms(self):
        self.publisher.is_publisher = True
        self.assertTrue(self.publisher.is_editor, True)
        self.assertTrue(self.publisher.is_staff, True)

    def test_editor_has_staff_perms(self):
        self.editor.is_editor = True
        self.assertTrue(self.editor.is_staff, True)

    def test_site_user_cannot_enter_admin_site_in_view(self):
        # Make sure site user cannot enter the admin site at all
        url = '/en/dashboard/'
        request = self.factory.get(url)
        request.user = self.site_user
        func, args, kwargs = resolve(url)
        response = func(request, args, **kwargs)
        # url for restricted users contains 'login' string
        self.assertIn('login', response.url)
