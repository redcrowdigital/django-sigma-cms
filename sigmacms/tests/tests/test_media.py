import os

from PIL import ImageDraw, Image
from authtools.models import User
from django.conf import settings
from django.core import files
from django.core.urlresolvers import reverse
from django.test import TestCase, Client
from filer.models import ClipboardItem, Clipboard, File
from filer.models.foldermodels import Folder


class MediaTestCase(TestCase):
    def setUp(self):
        self.admin = User.objects.create_superuser(email='admin@sigma.com', password='secret')
        self.publisher = User.objects.create_user(email='publisher@sigma.com', password='secret')
        self.publisher.is_publisher = True
        self.publisher.save()
        self.client = Client()
        self.client.login(email=self.admin.email, password='secret')
        self.image_name = 'file_name.png'
        self.image_path = os.path.join(os.path.dirname(__file__), 'files', self.image_name)
        self.image_file = files.File(open(self.image_path, 'rb'))
        self.clipboard = Clipboard.objects.get_or_create(user=self.admin)[0]

    def test_ajax_image_add_to_clipboard(self):
        response = self.client.post(
            reverse('admin:filer-ajax_upload') + "?qqfile=%s" % self.image_name,
            self.image_file.read(),
            'application/octet-stream',
            HTTP_X_REQUESTED_WITH='XMLHttpRequest'
        )
        clipboard_item = ClipboardItem.objects.first()
        # Make sure filename and owner saved properly
        self.assertEqual(clipboard_item.file.original_filename, self.image_name)
        self.assertEqual(clipboard_item.file.owner, self.admin)
        self.assertEqual(ClipboardItem.objects.count(), 1)

    def test_ajax_discard_clipboard(self):
        item = File.objects.create(owner=self.admin, original_filename=self.image_name, file=self.image_file)
        clipboard_item = ClipboardItem.objects.create(file=item, clipboard=self.clipboard)
        self.assertEqual(self.clipboard.files.count(), 1)
        response = self.client.post(
            reverse('admin:filer-discard_clipboard'),
            {
                'clipboard_id': self.clipboard.id
            },
            HTTP_X_REQUESTED_WITH='XMLHttpRequest'
        )
        self.assertEqual(response.status_code, 302)
        self.assertEqual(self.clipboard.files.count(), 0)

    def test_move_file_to_clipboard(self):
        item = File.objects.create(owner=self.admin, original_filename=self.image_name, file=self.image_file)
        self.assertEqual(self.clipboard.files.count(), 0)
        response = self.client.post(
            reverse('admin:filer-directory_listing-unfiled_images'),
            {
                'action': '',
                'select_across': 0,
                'move-to-clipboard-%s' % item.id: '',
            }
        )
        self.assertEqual(self.clipboard.files.count(), 1)

    def test_move_file_from_clipboard_to_folder(self):
        item = File.objects.create(owner=self.admin, original_filename=self.image_name, file=self.image_file)
        ClipboardItem.objects.create(clipboard=self.clipboard, file=item)
        folder = Folder.objects.create(name="Src", parent=None)
        self.assertEqual(self.clipboard.files.count(), 1)
        response = self.client.post(
            reverse('admin:filer-paste_clipboard_to_folder'),
            {
                'folder_id': folder.id,
                'clipboard_id': self.clipboard.id,
                'redirect_to': reverse('admin:filer-directory_listing', args=[folder.id])
            }
        )
        self.assertEqual(response.status_code, 302)
        self.assertEqual(self.clipboard.files.count(), 0)
        self.assertEqual(folder.files.count(), 1)

    def test_validate_no_duplicate_folders_on_move(self):
        """Create the following folder hierarchy:
        root
          |
          |--foo
          |   |-bar
          |
          |--bar

        and try to move the outer bar in foo. This has to fail since it would result
        in two folders with the same name and parent.
        """
        root = Folder.objects.create(name='root', owner=self.admin)
        foo = Folder.objects.create(name='foo', parent=root, owner=self.admin)
        bar = Folder.objects.create(name='bar', parent=root, owner=self.admin)
        foos_bar = Folder.objects.create(name='bar', parent=foo, owner=self.admin)
        response = self.client.post(
            reverse('admin:filer-directory_listing', args=[root.pk]),
            {
                'action': 'move_files_and_folders',
                'post': 'yes',
                'destination': foo.pk,
                '_selected_action': 'folder-%d' % bar.pk,
            })
        # refresh from db and validate that it hasn't been moved
        bar = Folder.objects.get(pk=bar.pk)
        self.assertEqual(bar.parent.pk, root.pk)

    def test_validate_no_duplicate_on_rename(self):
        root = Folder.objects.create(name='root', owner=self.admin)
        root2 = Folder.objects.create(name='root 2', owner=self.admin)
        response = self.client.post(
            reverse('admin:filer_folder_change', args=[root2.pk]),
            {
                'name': 'root',
                'owner': self.admin,
                '_save': '',
            })
        root2 = Folder.objects.get(pk=root2.pk)
        self.assertEqual(root2.name, 'root 2')
