import os

from authtools.models import User
from django.contrib.admin.options import get_content_type_for_model
from django.core import files
from django.core.exceptions import PermissionDenied
from django.core.urlresolvers import resolve, reverse
from django.test import TestCase, RequestFactory, Client
from filer.models import File
from fluent_contents.plugins.text.models import TextItem
from fluent_pages.models.db import UrlNode_Translation

from sigmacms.pagetypes.listnode.models import ListNode
from sigmacms.pagetypes.redirectnode.models import RedirectNode
from sigmacms.pagetypes.simplepage.models import SimplePage
from sigmacms.plugins.file.models import FileItem
from sigmacms.plugins.googlemap.models import GoogleMapItem
from sigmacms.plugins.oembeditem.models import OEmbedItem
from sigmacms.plugins.teaser.models import TeaserItem


class PageTestCase(TestCase):
    def setUp(self):
        self.admin = User.objects.create_superuser(email='admin@sigma.com', password='secret')
        self.publisher = User.objects.create_user(email='publisher@sigma.com', password='secret')
        self.publisher.is_publisher = True
        self.publisher.save()
        self.editor = User.objects.create_user(email='editor@sigma.com', password='secret')
        self.editor.is_editor = True
        self.publisher.save()
        self.site_user = User.objects.create_user(email='site_user@sigma.com', password='secret')
        self.rootPage1 = ListNode.objects.create(title='The first test root page', author=self.admin)
        self.rootPage1_es = UrlNode_Translation.objects.create(title='Title in Spanish', language_code='es', _cached_url='/title-in-spanish/', master=self.rootPage1)
        self.childPage1 = SimplePage.objects.create(title='The first test children page', parent=self.rootPage1, author=self.admin)
        self.rootPage2 = ListNode.objects.create(title='The second test root page', author=self.publisher)
        self.childPage2 = SimplePage.objects.create(title='The second test children page', parent=self.rootPage2, author=self.admin)
        self.factory = RequestFactory()
        self.client = Client(enforce_csrf_checks=False)
        self.client.login(email=self.admin.email, password='secret')
        self.page_url_publish = '/en/dashboard/fluent_pages/page/1/publish/'

    def test_can_inherit(self):
        # Make sure child page is nested in parent page
        self.assertEqual(self.childPage1.parent, self.rootPage1)

    def test_auto_slug(self):
        # Make sure page slug creates correctly by itself
        self.assertEqual(self.rootPage1.slug, 'the-first-test-root-page')

    def test_get_spanish_title(self):
        # Make sure page returns correct title in spanish
        self.rootPage1.set_current_language('es')
        self.assertEqual(self.rootPage1.title, self.rootPage1_es.title)

    def test_get_spanish_slug(self):
        # Make sure page slug returns in spanish properly
        self.rootPage1.set_current_language('es')
        self.assertEqual(self.rootPage1.slug, self.rootPage1_es.slug)

    def test_get_childs(self):
        # Make sure content method of the page returns child page
        self.assertIn(self.childPage1, self.rootPage1.content)

    def test_dont_get_another_childs(self):
        # Make sure content method of the page doesn't return another child page
        self.assertNotIn(self.childPage2, self.rootPage1.content)

    def test_site_user_permissions(self):
        # Make sure the site user has no rights to control the page
        # Is can_publish perm used at all?
        self.assertFalse(self.site_user.has_perm('simplepage.can_publish'))
        self.assertFalse(self.site_user.has_perm('simplepage.can_add'))
        self.assertFalse(self.site_user.has_perm('simplepage.can_edit'))
        self.assertFalse(self.site_user.has_perm('simplepage.can_delete'))

    def test_publisher_user_permissions(self):
        # Make sure the publisher has all control to the page
        self.assertTrue(self.publisher.has_perm('simplepage.can_delete'))
        self.assertTrue(self.publisher.has_perm('simplepage.can_edit'))
        self.assertTrue(self.publisher.has_perm('simplepage.can_add'))

    def test_editor_user_permissions(self):
        # Make sure the publisher has all control to the page
        self.assertTrue(self.editor.has_perm('simplepage.can_delete'))
        self.assertTrue(self.editor.has_perm('simplepage.can_edit'))
        self.assertTrue(self.editor.has_perm('simplepage.can_add'))

    def test_superuser_page_permissions(self):
        self.assertTrue(self.admin.has_perm('simplepage.can_delete'))
        self.assertTrue(self.admin.has_perm('simplepage.can_edit'))
        self.assertTrue(self.admin.has_perm('simplepage.can_add'))

    def test_editor_user_cannot_publish_page_in_view(self):
        request = self.factory.get(self.page_url_publish)
        request.user = self.editor
        func, args, kwargs = resolve(self.page_url_publish)
        self.assertRaises(PermissionDenied, func, request, *args, **kwargs)

    def test_publisher_user_can_publish_page_in_view(self):
        self.client.login(email=self.publisher.email, password='secret')
        response = self.client.get(self.page_url_publish)
        # if we have 302 status code the page has published properly
        self.assertTrue(response.status_code, 302)

    def test_superuser_can_publish_page_in_view(self):
        self.client.login(email=self.publisher.email, password='secret')
        response = self.client.get(self.page_url_publish)
        # if we have 302 status code the page has published properly
        self.assertTrue(response.status_code, 302)

    def test_published_page_can_be_visited_by_url(self):
        self.rootPage1.publish()
        response = self.client.get(self.rootPage1.get_absolute_url())
        self.assertTrue(response.status_code, 200)

    def test_draft_page_cannot_be_visited_by_url(self):
        self.rootPage1.unpublish()
        response = self.client.get(self.rootPage1.get_absolute_url())
        self.assertTrue(response.status_code, 404)

    def test_created_page_has_draft_status(self):
        self.assertTrue(self.rootPage1.is_draft)

    def test_page_can_be_published(self):
        self.rootPage1.publish()
        self.assertTrue(self.rootPage1.published)
        self.assertTrue(self.rootPage1.publisher_linked.is_published)
        self.assertFalse(self.rootPage1.publisher_linked.is_draft)

    def test_published_page_copy(self):
        self.rootPage1.publish()
        self.assertNotEqual(self.rootPage1.pk, self.rootPage1.publisher_linked.pk)

    def test_published_page_has_publication_time(self):
        self.rootPage1.publish()
        self.assertTrue(self.rootPage1.publisher_published_at)

    def test_page_move_inside_page_ajax(self):
        self.client.login(email=self.admin.email, password='secret')
        response = self.client.post(
            reverse('admin:fluent_pages_page_moved'),
            {
                'moved_id': self.childPage2.id,
                'position': 'inside',
                'target_id': self.rootPage1.id,
                'previous_parent_id': self.rootPage2.id,
            },
            HTTP_X_REQUESTED_WITH='XMLHttpRequest'
        )
        root_page = self.rootPage1.__class__.objects.get(pk=self.rootPage1.pk)
        children_page = self.childPage2.__class__.objects.get(pk=self.childPage2.pk)
        self.assertEqual(response.status_code, 200)
        self.assertIn(children_page, root_page.content)
        self.assertEqual(children_page.parent, root_page)

    def test_page_move_before_page_ajax(self):
        self.childPage1.parent = self.rootPage1
        self.childPage1.save()
        response = self.client.post(
            reverse('admin:fluent_pages_page_moved'),
            {
                'moved_id': self.childPage1.id,
                'position': 'before',
                'target_id': self.rootPage1.id,
                'previous_parent_id': self.rootPage1.id,
             },
            HTTP_X_REQUESTED_WITH='XMLHttpRequest'
        )
        root_page = self.rootPage1.__class__.objects.get(pk=self.rootPage1.pk)
        children_page = self.childPage1.__class__.objects.get(pk=self.childPage1.pk)
        self.assertEqual(response.status_code, 200)
        self.assertNotIn(children_page, root_page.content)
        self.assertNotEqual(children_page.parent, root_page)
        self.assertLess(children_page.tree_id, root_page.tree_id)

    def test_page_move_after_page_ajax(self):
        self.childPage1.parent = self.rootPage1
        self.childPage1.save()
        response = self.client.post(
            reverse('admin:fluent_pages_page_moved'),
            {
                'moved_id': self.childPage1.id,
                'position': 'after',
                'target_id': self.rootPage1.id,
                'previous_parent_id': self.rootPage1.id
            }
        )
        root_page = self.rootPage1.__class__.objects.get(pk=self.rootPage1.pk)
        children_page = self.childPage1.__class__.objects.get(pk=self.childPage1.pk)
        self.assertEqual(response.status_code, 200)
        self.assertNotIn(children_page, root_page.content)
        self.assertNotEqual(children_page.parent, root_page)
        self.assertGreater(children_page.tree_id, root_page.tree_id)

    def test_page_add_html_text(self):
        model_ct = get_content_type_for_model(self.childPage1.__class__)
        text = '<foo>bar</foo>'
        text_item = TextItem.objects.create(text=text, parent_type=model_ct, parent_id=self.childPage1.pk)
        self.assertEqual(self.childPage1.contentitem_set.count(), 1)

    def test_page_add_google_map(self):
        model_ct = get_content_type_for_model(self.childPage1.__class__)
        google_map = GoogleMapItem.objects.create(
            title='Test Map',
            address='Brodie ln',
            zipcode='78745',
            city='Austin',
            zoom=14,
            width=350,
            height=350,
            mode='i',
            parent_type=model_ct,
            parent_id=self.childPage1.pk
        )
        self.assertEqual(self.childPage1.contentitem_set.count(), 1)

    def test_page_add_file(self):
        model_ct = get_content_type_for_model(self.childPage1.__class__)
        image_name = 'file_name.png'
        image_file = files.File(open(os.path.join(os.path.dirname(__file__), 'files', image_name), 'rb'))
        item = File.objects.create(owner=self.admin, original_filename=image_name, file=image_file)
        file_item = FileItem.objects.create(
            file=item,
            title='test image',
            parent_type=model_ct,
            parent_id=self.childPage1.pk
        )
        self.assertEqual(self.childPage1.contentitem_set.count(), 1)

    def test_page_teaser(self):
        model_ct = get_content_type_for_model(self.childPage1.__class__)
        teaser = TeaserItem.objects.create(
            title='test teaser',
            parent_type=model_ct,
            parent_id=self.childPage1.pk,
            target='_blank',
            url='http',
        )
        self.assertEqual(self.childPage1.contentitem_set.count(), 1)

    def test_page_media(self):
        model_ct = get_content_type_for_model(self.childPage1.__class__)
        teaser = OEmbedItem.objects.create(
            height=350,
            width=350,
            title='Test media',
            embed_url='https://youtu.be/NABXU28sksE',
            parent_type=model_ct,
            parent_id=self.childPage1.pk
        )
        self.assertEqual(self.childPage1.contentitem_set.count(), 1)

    def test_redirect_page(self):
        new_url = 'http://google.com'
        redirect_page = RedirectNode.objects.create(author=self.admin, title='Redirect Page', new_url=new_url)
        redirect_page.publish()
        response = self.client.get(redirect_page.get_absolute_url())
        self.assertEqual(response.status_code, 302)
        self.assertEqual(response.url, new_url)

    def test_page_can_be_deleted(self):
        response = self.client.post(
            reverse('admin:simplepage_simplepage_delete', args=[self.childPage1.pk]),
            {'post': 'yes'}
        )
        self.assertEqual(response.status_code, 302)
        self.assertFalse(self.childPage1.__class__.objects.filter(pk=self.childPage1.pk))

    def test_list_page_queryset(self):
        list_page = ListNode.objects.create(title='parent list page', author=self.admin)
        list_page.publish()
        detail_page1 = SimplePage.objects.create(title='first page', parent=list_page, author=self.admin)
        detail_page1.publish()
        detail_page2 = SimplePage.objects.create(parent=list_page, title='the second page', author=self.admin)
        detail_page2.publish()
        detail_page3 = SimplePage.objects.create(title='the third page', parent=list_page, author=self.admin)
        detail_page3.publish()
        detail_page4 = SimplePage.objects.create(title='the fourth page', parent=list_page, author=self.admin)
        detail_page4.publish()
        detail_page5 = SimplePage.objects.create(title='the fifth page', author=self.admin)
        detail_page5.publish()
        response = self.client.get(list_page.get_absolute_url())
        self.assertIn(detail_page1.publisher_linked, response.context['page'].content)
        self.assertNotIn(detail_page5.publisher_linked, response.context['page'].content)