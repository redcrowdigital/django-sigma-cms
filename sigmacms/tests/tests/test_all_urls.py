from unittest import TestCase

from authtools.models import User
from django.core.urlresolvers import reverse
from django.test import Client


class UrlsTestCase(TestCase):
    def setUp(self):
        self.admin = User.objects.create_superuser(email='admin@sigma.com', password='secret')
        self.client = Client()
        self.client.login(username=self.admin.email, password='secret')

    def test_common_url(self):
        # test all admin urls without any kwargs just to be sured they renders correctly
        url_list = [
            'admin:auth_group_changelist',
            'admin:auth_group_add',
            'admin:filer_clipboard_changelist',
            'admin:filer_clipboard_add',
            'admin:auth_group_add',
            'admin:authtools_user_changelist',
            'admin:authtools_user_add',
            'admin:filer_clipboard_changelist',
            'admin:filer_clipboard_add',
            'admin:filer-delete_clipboard',
            'admin:filer-discard_clipboard',
            'admin:filer-paste_clipboard_to_folder',
            'admin:filer-ajax_upload',
            'admin:filer_file_changelist',
            'admin:filer_file_add',
            'admin:filer_folder_changelist',
            'admin:filer-directory_listing-root',
            'admin:filer_folder_add',
            'admin:filer-directory_listing-images_with_missing_data',
            'admin:filer-directory_listing-last',
            'admin:filer-directory_listing-make_root_folder',
            'admin:filer-directory_listing-unfiled_images',
            'admin:filer_folderpermission_changelist',
            'admin:filer_folderpermission_add',
            'admin:filer_image_changelist',
            'admin:filer_image_add',
            'admin:filerpicker',
            'admin:fluent_pages_page_changelist',
            'admin:listnode_listnode_changelist',
            'admin:redirectnode_redirectnode_changelist',
            'admin:simplepage_simplepage_changelist',
            'admin:listnode_listnode_add',
            'admin:redirectnode_redirectnode_add',
            'admin:simplepage_simplepage_add',
            'admin:fluent_pages_page_add',
            'admin:fluent_pages_page_moved',
            'admin:fluent_pages_pagelayout_changelist',
            'admin:fluent_pages_pagelayout_add',
            'admin:jsi18n',
            'admin:login',
            'admin:logout',
            'admin:password_change',
            'admin:password_change_done',
            'admin:post_office_email_changelist',
            'admin:post_office_email_add',
            'admin:post_office_emailtemplate_changelist',
            'admin:post_office_emailtemplate_add',
            'admin:post_office_log_changelist',
            'admin:post_office_log_add',
            'admin:sharedcontent_sharedcontent_add',
            'admin:sites_site_changelist',
            'admin:sites_site_add',
            'admin:sitesettings_sitesettings_changelist',
            'admin:sitesettings_sitesettings_add',
            'favicon',
        ]

        for url in url_list:
            response = self.client.get(reverse(url))
            self.assertTrue(response.status_code, 200)