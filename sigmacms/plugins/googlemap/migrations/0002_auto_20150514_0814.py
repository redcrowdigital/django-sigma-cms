# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('googlemap', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='googlemapitem',
            name='address',
            field=models.CharField(default='1600 Amphitheatre Parkway', max_length=150, verbose_name='address'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='googlemapitem',
            name='city',
            field=models.CharField(default='Mountain View', max_length=100, verbose_name='city'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='googlemapitem',
            name='lat',
            field=models.DecimalField(decimal_places=6, max_digits=10, blank=True, help_text='Use latitude & longitude to fine tune the map position.', null=True, verbose_name='latitude'),
        ),
        migrations.AddField(
            model_name='googlemapitem',
            name='lng',
            field=models.DecimalField(null=True, verbose_name='longitude', max_digits=10, decimal_places=6, blank=True),
        ),
        migrations.AddField(
            model_name='googlemapitem',
            name='title',
            field=models.CharField(max_length=100, null=True, verbose_name='map title', blank=True),
        ),
        migrations.AddField(
            model_name='googlemapitem',
            name='zipcode',
            field=models.CharField(default='CA 94043', max_length=30, verbose_name='zip code'),
            preserve_default=False,
        ),
    ]
