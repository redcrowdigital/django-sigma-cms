# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('googlemap', '0005_auto_20150518_0548'),
    ]

    operations = [
        migrations.AlterField(
            model_name='googlemapitem',
            name='mode',
            field=models.CharField(default=b'i', max_length=1, verbose_name='mode', choices=[(b's', 'static'), (b'i', 'interactive')]),
        ),
        migrations.AlterField(
            model_name='googlemapitem',
            name='title',
            field=models.CharField(max_length=100, null=True, verbose_name='title', blank=True),
        ),
    ]
