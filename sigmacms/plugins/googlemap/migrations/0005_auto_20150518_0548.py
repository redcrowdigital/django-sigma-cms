# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('googlemap', '0004_googlemapitem_zoom'),
    ]

    operations = [
        migrations.AddField(
            model_name='googlemapitem',
            name='mode',
            field=models.CharField(default=b'i', max_length=1, verbose_name='map mode', choices=[(b's', 'static'), (b'i', 'interactive')]),
        ),
        migrations.AlterField(
            model_name='googlemapitem',
            name='zoom',
            field=models.PositiveSmallIntegerField(default=14, verbose_name='zoom level', choices=[(0, b'0'), (1, b'1'), (2, b'2'), (3, b'3'), (4, b'4'), (5, b'5'), (6, b'6'), (7, b'7'), (8, b'8'), (9, b'9'), (10, b'10'), (11, b'11'), (12, b'12'), (13, b'13'), (14, b'14'), (15, b'15'), (16, b'16'), (17, b'17'), (18, b'18'), (19, b'19'), (20, b'20'), (21, b'21')]),
        ),
    ]
