# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('googlemap', '0002_auto_20150514_0814'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='googlemapitem',
            name='lat',
        ),
        migrations.RemoveField(
            model_name='googlemapitem',
            name='lng',
        ),
        migrations.AddField(
            model_name='googlemapitem',
            name='height',
            field=models.PositiveIntegerField(null=True, verbose_name='height', blank=True),
        ),
        migrations.AddField(
            model_name='googlemapitem',
            name='width',
            field=models.PositiveIntegerField(null=True, verbose_name='width', blank=True),
        ),
    ]
