# -*- coding: utf-8 -*-

from django.contrib.admin.widgets import AdminTextInputWidget
from django.utils.translation import ugettext_lazy as _

from fluent_contents.extensions import ContentPlugin, plugin_pool

from .models import GoogleMapItem


@plugin_pool.register
class GoogleMapPlugin(ContentPlugin):
    model = GoogleMapItem
    category = _('Media')
    admin_form_template = "admin/fluent_contents/plugins/googlemap/admin_form.html"
    render_template = "fluent_contents/plugins/googlemap/googlemap.html"

    formfield_overrides = {
        'width': {
            'widget': AdminTextInputWidget(attrs={'class': 'vTextField mWidthField'}),
        },
        'height': {
            'widget': AdminTextInputWidget(attrs={'class': 'vTextField mHeightField'}),
        },
    }

    fieldsets = (
        (None, {
            'fields': (
                'title',
                'address',
                'zipcode',
                'city',
                'zoom',
                ('width', 'height'),
                'mode',
            ),
        }),
    )

    class Media:
        css = {
            'screen': (
                'fluent_contents/plugins/googlemap/googlemap_admin.css',
            )
        }
