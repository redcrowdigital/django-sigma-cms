# -*- coding: utf-8 -*-

from future.utils import python_2_unicode_compatible

from django.db import models
from django.utils.translation import ugettext_lazy as _

from fluent_contents.models import ContentItem


@python_2_unicode_compatible
class GoogleMapItem(ContentItem):
    STATIC = 's'
    INTERACTIVE = 'i'
    MODES = ((STATIC, _("static")), (INTERACTIVE, _("interactive")))

    title = models.CharField(_("title"), max_length=100, blank=True, null=True)

    address = models.CharField(_("address"), max_length=150)
    zipcode = models.CharField(_("zip code"), max_length=30)
    city = models.CharField(_("city"), max_length=100)

    ZOOM_LEVELS = map(lambda c: (c, str(c)), range(22))
    zoom = models.PositiveSmallIntegerField(
        _("zoom level"), choices=ZOOM_LEVELS, default=14)
    
    width = models.PositiveIntegerField(_("width"), blank=True, null=True)
    height = models.PositiveIntegerField(_("height"), blank=True, null=True)

    mode = models.CharField(_("mode"), choices=MODES, default=INTERACTIVE, max_length=1)
    
    class Meta:
        verbose_name = _("Google Map")
        verbose_name_plural = _("Google Map")

    def __str__(self):
        return u"%s (%s)" % (self.get_title(), self.get_address())

    def get_title(self):
        if self.title is None:
            return _("Map")
        return self.title

    def get_address(self):
        return "%s, %s, %s" % (self.address, self.zipcode, self.city)

    def get_width(self):
        if self.width is None:
            return 640  # default value
        return int(self.width)

    def get_height(self):
        if self.height is None:
            return 480
        return int(self.height)

    def is_static(self):
        return self.mode == self.STATIC
