# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('fluent_contents', '0001_initial'),
        ('fluent_pages', '0005_auto_20150720_0559'),
    ]

    operations = [
        migrations.CreateModel(
            name='LinkItem',
            fields=[
                ('contentitem_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='fluent_contents.ContentItem')),
                ('name', models.CharField(max_length=256, verbose_name='name')),
                ('url', models.URLField(null=True, verbose_name='link', blank=True)),
                ('mailto', models.EmailField(help_text='An email adress has priority over a text link.', max_length=75, null=True, verbose_name='mailto', blank=True)),
                ('target', models.CharField(blank=True, max_length=100, verbose_name='target', choices=[(b'', 'same window'), (b'_blank', 'new window'), (b'_parent', 'parent window'), (b'_top', 'topmost frame')])),
                ('page_link', models.ForeignKey(blank=True, to='fluent_pages.Page', help_text='A link to a page has priority over a text link.', null=True, verbose_name='page')),
            ],
            options={
                'db_table': 'contentitem_link_linkitem',
                'verbose_name': 'Link',
                'verbose_name_plural': 'Links',
            },
            bases=('fluent_contents.contentitem',),
        ),
    ]
