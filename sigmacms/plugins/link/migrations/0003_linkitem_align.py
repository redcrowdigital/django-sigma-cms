# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('link', '0002_auto_20150814_1121'),
    ]

    operations = [
        migrations.AddField(
            model_name='linkitem',
            name='align',
            field=models.CharField(blank=True, max_length=10, verbose_name='Align', choices=[(b'left', 'Left'), (b'center', 'Center'), (b'right', 'Right')]),
            preserve_default=True,
        ),
    ]
