# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import fluent_contents.extensions


class Migration(migrations.Migration):

    dependencies = [
        ('link', '0001_initial'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='linkitem',
            name='page_link',
        ),
        migrations.AlterField(
            model_name='linkitem',
            name='mailto',
            field=models.EmailField(help_text='An email adress has priority over a text or page link.', max_length=75, null=True, verbose_name='mailto', blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='linkitem',
            name='url',
            field=fluent_contents.extensions.PluginUrlField(max_length=300, null=True, verbose_name='URL', blank=True),
            preserve_default=True,
        ),
    ]
