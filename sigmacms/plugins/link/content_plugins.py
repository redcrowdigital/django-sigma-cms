# -*- coding: utf-8 -*-

from django.utils.translation import ugettext_lazy as _

from fluent_contents.extensions import ContentPlugin, plugin_pool

from .models import LinkItem


@plugin_pool.register
class LinkPlugin(ContentPlugin):
    """
    A link to an other page or to an external website
    """
    model = LinkItem
    render_template = "fluent_contents/plugins/link/default.html"

    radio_fields = {
        'align': ContentPlugin.HORIZONTAL
    }

    def get_context(self, request, instance, **kwargs):
        if instance.mailto:
            link = u"mailto:%s" % instance.mailto
        elif instance.url:
            link = instance.url
        else:
            link = ""
        context = super(LinkPlugin, self).get_context(request, instance, **kwargs)

        context.update({
            'link': link, 
            'target': instance.target,
        })
        return context    
