# -*- coding: utf-8 -*-

from django.utils.translation import ugettext_lazy as _

from fluent_contents.extensions import ContentPlugin, plugin_pool

from .models import TwitterRecentEntriesItem, TwitterSearchItem


@plugin_pool.register
class TwitterRecentEntriesPlugin(ContentPlugin):
    model = TwitterRecentEntriesItem
    render_template = "fluent_contents/plugins/twitterfeed/recent_entries.html"
    category = _('Media')    

    
@plugin_pool.register
class TwitterSearchPlugin(ContentPlugin):
    model = TwitterSearchItem
    render_template = "fluent_contents/plugins/twitterfeed/search.html"
    category = _('Media')
