# -*- coding: utf-8 -*-

from future.utils import python_2_unicode_compatible

from django.db import models
from django.utils.translation import ugettext_lazy as _

from fluent_contents.models import ContentItem


@python_2_unicode_compatible
class TwitterRecentEntriesItem(ContentItem):

    title = models.CharField(_('title'), max_length=75, blank=True)

    twitter_user = models.CharField(_('twitter user'), max_length=75)
    amount = models.PositiveSmallIntegerField(_('Number of results'), default=5)

    twitter_id = models.CharField(_('widget id'), max_length=75,
        help_text=_(u'See <a href="https://twitter.com/settings/widgets" target="_blank">https://twitter.com/settings/widgets</a> on how to obtain one'))
    
    class Meta:
        verbose_name = _('Recent twitter entries')
        verbose_name_plural = _('Recent twitter entries')

    def __str__(self):
        return self.title or self.twitter_user
    

@python_2_unicode_compatible
class TwitterSearchItem(ContentItem):

    title = models.CharField(_('title'), max_length=75, blank=True)

    query = models.CharField(_('query'), max_length=200, blank=True, default='',
        help_text=_('Deprecated: no longer used by Twitter widgets. Define one when creating widgets.'))
    amount = models.PositiveSmallIntegerField(_('Number of results'), default=5)

    twitter_id = models.CharField(_('widget id'), max_length=75,
        help_text=_(u'See <a href="https://twitter.com/settings/widgets" target="_blank">https://twitter.com/settings/widgets</a> on how to obtain one'))    

    class Meta:
        verbose_name = _('Twitter search feed')
        verbose_name_plural = _('Twitter search feed')    

    def __str__(self):
        return self.title or self.query
           
