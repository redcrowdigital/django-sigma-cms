# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('twitterfeed', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='twitterrecententriesitem',
            name='twitter_id',
            field=models.CharField(help_text='See <a href="https://twitter.com/settings/widgets" target="_blank">https://twitter.com/settings/widgets</a> on how to obtain one', max_length=75, verbose_name='widget id'),
        ),
        migrations.AlterField(
            model_name='twittersearchitem',
            name='twitter_id',
            field=models.CharField(help_text='See <a href="https://twitter.com/settings/widgets" target="_blank">https://twitter.com/settings/widgets</a> on how to obtain one', max_length=75, verbose_name='widget id'),
        ),
    ]
