# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('fluent_contents', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='TwitterRecentEntriesItem',
            fields=[
                ('contentitem_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='fluent_contents.ContentItem')),
                ('title', models.CharField(max_length=75, verbose_name='title', blank=True)),
                ('twitter_user', models.CharField(max_length=75, verbose_name='twitter user')),
                ('amount', models.PositiveSmallIntegerField(default=5, verbose_name='Number of results')),
                ('twitter_id', models.CharField(help_text='See https://twitter.com/widgets on how to obtain one', max_length=75, verbose_name='twitter id')),
            ],
            options={
                'db_table': 'contentitem_twitterfeed_twitterrecententriesitem',
                'verbose_name': 'Recent twitter entries',
                'verbose_name_plural': 'Recent twitter entries',
            },
            bases=('fluent_contents.contentitem',),
        ),
        migrations.CreateModel(
            name='TwitterSearchItem',
            fields=[
                ('contentitem_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='fluent_contents.ContentItem')),
                ('title', models.CharField(max_length=75, verbose_name='title', blank=True)),
                ('query', models.CharField(default=b'', help_text='Deprecated: no longer used by Twitter widgets. Define one when creating widgets.', max_length=200, verbose_name='query', blank=True)),
                ('amount', models.PositiveSmallIntegerField(default=5, verbose_name='Number of results')),
                ('twitter_id', models.CharField(help_text='See https://twitter.com/widgets on how to obtain one', max_length=75, verbose_name='twitter id')),
            ],
            options={
                'db_table': 'contentitem_twitterfeed_twittersearchitem',
                'verbose_name': 'Twitter search feed',
                'verbose_name_plural': 'Twitter search feed',
            },
            bases=('fluent_contents.contentitem',),
        ),
    ]
