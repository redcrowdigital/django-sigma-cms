# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import filer.fields.image
import fluent_contents.extensions


class Migration(migrations.Migration):

    dependencies = [
        ('filer', '0002_auto_20150606_2003'),
        ('fluent_contents', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='TeaserItem',
            fields=[
                ('contentitem_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='fluent_contents.ContentItem')),
                ('title', models.CharField(max_length=256, verbose_name='title')),
                ('description', models.TextField(null=True, verbose_name='description', blank=True)),
                ('url', fluent_contents.extensions.PluginUrlField(max_length=300, null=True, verbose_name='URL', blank=True)),
                ('target', models.CharField(blank=True, max_length=100, verbose_name='target', choices=[(b'', 'same window'), (b'_blank', 'new window'), (b'_parent', 'parent window'), (b'_top', 'topmost frame')])),
                ('image', filer.fields.image.FilerImageField(blank=True, to='filer.Image', null=True)),
            ],
            options={
                'db_table': 'contentitem_teaser_teaseritem',
                'verbose_name': 'Teaser',
                'verbose_name_plural': 'Teaser',
            },
            bases=('fluent_contents.contentitem',),
        ),
    ]
