# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import sigmacms.plugins.oembeditem.fields


class Migration(migrations.Migration):

    dependencies = [
        ('oembeditem', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='oembeditem',
            name='caption',
            field=models.TextField(verbose_name='Caption', blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='oembeditem',
            name='embed_url',
            field=sigmacms.plugins.oembeditem.fields.OEmbedUrlField(help_text='Enter the URL of the online content to embed (e.g. a YouTube or Vimeo video, SlideShare presentation, etc..)', verbose_name='URL to embed'),
            preserve_default=True,
        ),
    ]
