# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import filer.fields.image


class Migration(migrations.Migration):

    dependencies = [
        ('filer', '0002_auto_20150606_2003'),
        ('fluent_contents', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Slide',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=255)),
                ('content', models.TextField(blank=True)),
                ('url', models.URLField(help_text='If present, clicking on image will take user to link.', null=True, verbose_name='link', blank=True)),
                ('priority', models.IntegerField(default=100, help_text='Priority display value', verbose_name='display priority')),
                ('creation_date', models.DateTimeField(auto_now_add=True)),
                ('image', filer.fields.image.FilerImageField(to='filer.Image')),
            ],
            options={
                'verbose_name': 'Slide',
                'verbose_name_plural': 'Slides',
            },
        ),
        migrations.CreateModel(
            name='Slideshow',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=255)),
                ('slug', models.SlugField(unique=True, max_length=75)),
                ('transition_time', models.IntegerField(default=0, help_text='Sets the amount of time in milliseconds before transitioning a slide. Set 0 to use default value.', null=True, blank=True)),
                ('creation_date', models.DateTimeField(auto_now_add=True)),
            ],
            options={
                'verbose_name': 'Slideshow',
                'verbose_name_plural': 'Slideshows',
            },
        ),
        migrations.CreateModel(
            name='SlideshowItem',
            fields=[
                ('contentitem_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='fluent_contents.ContentItem')),
                ('slideshow', models.ForeignKey(to='slideshow.Slideshow')),
            ],
            options={
                'db_table': 'contentitem_slideshow_slideshowitem',
                'verbose_name': 'Slideshow',
                'verbose_name_plural': 'Slideshow',
            },
            bases=('fluent_contents.contentitem',),
        ),
        migrations.AddField(
            model_name='slide',
            name='slideshow',
            field=models.ForeignKey(to='slideshow.Slideshow'),
        ),
    ]
