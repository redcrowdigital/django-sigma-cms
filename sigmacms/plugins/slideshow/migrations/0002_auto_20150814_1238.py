# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import fluent_contents.extensions


class Migration(migrations.Migration):

    dependencies = [
        ('slideshow', '0001_initial'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='slide',
            options={'ordering': ['priority'], 'verbose_name': 'Slide', 'verbose_name_plural': 'Slides'},
        ),
        migrations.AlterField(
            model_name='slide',
            name='content',
            field=models.TextField(verbose_name='content', blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='slide',
            name='creation_date',
            field=models.DateTimeField(auto_now_add=True, verbose_name='creation date'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='slide',
            name='slideshow',
            field=models.ForeignKey(verbose_name='slideshow', to='slideshow.Slideshow'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='slide',
            name='title',
            field=models.CharField(max_length=255, verbose_name='title'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='slide',
            name='url',
            field=fluent_contents.extensions.PluginUrlField(help_text='If present, clicking on image will take user to link.', max_length=300, null=True, verbose_name='URL', blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='slideshow',
            name='creation_date',
            field=models.DateTimeField(auto_now_add=True, verbose_name='creation date'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='slideshow',
            name='slug',
            field=models.SlugField(unique=True, max_length=75, verbose_name='slug'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='slideshow',
            name='title',
            field=models.CharField(max_length=255, verbose_name='title'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='slideshow',
            name='transition_time',
            field=models.IntegerField(default=0, help_text='Sets the amount of time in milliseconds before transitioning a slide. Set 0 to use default value.', null=True, verbose_name='transition time', blank=True),
            preserve_default=True,
        ),
    ]
