# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import filer.fields.image


class Migration(migrations.Migration):

    dependencies = [
        ('filer', '0002_auto_20150606_2003'),
        ('slideshow', '0002_auto_20150814_1238'),
    ]

    operations = [
        migrations.AddField(
            model_name='slide',
            name='mobile_image',
            field=filer.fields.image.FilerImageField(related_name='+', blank=True, to='filer.Image', help_text='Alternate image for mobile.', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='slide',
            name='video_url',
            field=models.URLField(help_text='Video embed URL.', verbose_name='Video URL', blank=True),
            preserve_default=True,
        ),
    ]
