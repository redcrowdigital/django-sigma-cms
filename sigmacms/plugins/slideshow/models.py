# -*- coding: utf-8 -*-

from future.utils import python_2_unicode_compatible

from django.db import models
from django.db.models import URLField
from django.utils.translation import ugettext_lazy as _

from fluent_contents.extensions import PluginUrlField

from filer.fields.image import FilerImageField
from fluent_contents.models import ContentItem


@python_2_unicode_compatible
class Slide(models.Model):

    slideshow = models.ForeignKey('slideshow.Slideshow', verbose_name=_('slideshow'))

    title = models.CharField(_('title'), blank=False, max_length=255)
    content = models.TextField(_('content'), blank=True)

    image = FilerImageField()

    mobile_image = FilerImageField(blank=True, null=True,
        help_text=_("Alternate image for mobile."), related_name='+')

    url = PluginUrlField(_("URL"), blank=True, null=True,
        help_text=_("If present, clicking on image will take user to link."))

    video_url = URLField(_("Video URL"), blank=True,
        help_text=_("Video embed URL."))

    priority = models.IntegerField(_('display priority'), default=100, help_text=_('Priority display value'))

    creation_date = models.DateTimeField(_('creation date'), editable=False, auto_now_add=True)
    
    class Meta:
        verbose_name = _("Slide")
        verbose_name_plural = _("Slides")
        ordering = ['priority']
        
    def __str__(self):
        return self.title


@python_2_unicode_compatible
class Slideshow(models.Model):

    title = models.CharField(_('title'), blank=False, max_length=255)
    slug = models.SlugField(_('slug'), unique=True, max_length=75)

    transition_time = models.IntegerField(
        _('transition time'), default=0, null=True, blank=True,
        help_text=_("Sets the amount of time in milliseconds before transitioning a slide. "
                    "Set 0 to use default value."),
    )

    creation_date = models.DateTimeField(_('creation date'), editable=False, auto_now_add=True)

    class Meta:
        verbose_name = _("Slideshow")
        verbose_name_plural = _("Slideshows")

    def __str__(self):
        return self.title


@python_2_unicode_compatible
class SlideshowItem(ContentItem):

    slideshow = models.ForeignKey(Slideshow)
    
    class Meta:
        verbose_name = _("Slideshow")
        verbose_name_plural = _("Slideshow")

    @property
    def slides(self):
        return self.slideshow.slide_set.all().order_by('priority')
        
    def __str__(self):
        return self.slideshow.title
