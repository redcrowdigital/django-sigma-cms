# -*- coding: utf-8 -*-

from django.utils.translation import ugettext_lazy as _

from fluent_contents.extensions import ContentPlugin, plugin_pool

from .models import SlideshowItem


@plugin_pool.register
class SlideshowPlugin(ContentPlugin):
    model = SlideshowItem
    category = _('Media')
    render_template = "fluent_contents/plugins/slideshow/slideshow.html"
