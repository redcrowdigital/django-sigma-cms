# -*- coding: utf-8 -*-

from django.contrib import admin
from django.contrib.admin import ModelAdmin
from django.utils.translation import ugettext_lazy as _

from sigmacms.admin.mixins import StaffAdminMixin, ForeignKeyAdminMixin

from .models import Slideshow, Slide


class SlideInline(StaffAdminMixin, admin.StackedInline):
    model = Slide
    extra = 0


class SlideshowAdmin(StaffAdminMixin, ForeignKeyAdminMixin, ModelAdmin):
    ordering = ('title',)
    search_fields = ('title',)
    list_filter = ('creation_date',)
    prepopulated_fields = {"slug": ("title",)}
    list_display = ('slug', 'title', 'count', 'creation_date')
    fieldsets = (
        (None, {
            'classes': ['wide',],
            'fields': ('title', 'slug'),
        }),
    )
    inlines = [SlideInline]

    def count(self, obj):
        return obj.slide_set.count()
    count.short_description = _('No. of slides')
    count.admin_order_field = 'slides__count'


class SlideAdmin(StaffAdminMixin, ForeignKeyAdminMixin, ModelAdmin):
    ordering = ('slideshow__slug',)
    search_fields = ('title', 'content')
    list_filter = ('creation_date', 'slideshow')
    list_display = ('title', 'slideshow', 'creation_date')
    enhance_exclude = ['image']
    fieldsets = (
        (None, {
            'fields': ('slideshow',),
        }),
        (None, {
            'fields': ('title', 'image', 'url')
        }),
        (None, {
            'fields': ('content',),
        }),
    )

admin.site.register(Slideshow, SlideshowAdmin)
admin.site.register(Slide, SlideAdmin)
