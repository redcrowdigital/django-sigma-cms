# -*- coding: utf-8 -*-

from future.builtins import str
from future.utils import python_2_unicode_compatible

from django.db import models
from django.utils.translation import ugettext_lazy as _

from fluent_contents.models.db import ContentItem

from filer.fields.file import FilerFileField


@python_2_unicode_compatible
class FileItem(ContentItem):

    file = FilerFileField()
    title = models.CharField(_("title"), max_length=255, null=True, blank=True)

    class Meta:
        verbose_name = _("File")
        verbose_name_plural = _("Files")

    def __str__(self):
        if self.title:
            return self.title
        elif self.file:
            return str(self.file)
        return "<empty>"
