# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import filer.fields.file


class Migration(migrations.Migration):

    dependencies = [
        ('filer', '0002_auto_20150606_2003'),
        ('fluent_contents', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='FileItem',
            fields=[
                ('contentitem_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='fluent_contents.ContentItem')),
                ('title', models.CharField(max_length=255, null=True, verbose_name='title', blank=True)),
                ('file', filer.fields.file.FilerFileField(to='filer.File')),
            ],
            options={
                'db_table': 'contentitem_file_fileitem',
                'verbose_name': 'File',
                'verbose_name_plural': 'Files',
            },
            bases=('fluent_contents.contentitem',),
        ),
    ]
