# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('fluent_pages', '0002_auto_20150511_0618'),
    ]

    operations = [
        migrations.AddField(
            model_name='urlnode',
            name='publisher_published_at',
            field=models.DateTimeField(verbose_name='publshed at', null=True, editable=False),
        ),
    ]
