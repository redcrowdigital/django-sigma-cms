# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.db.models.deletion
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('fluent_pages', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='urlnode',
            name='publisher_is_draft',
            field=models.BooleanField(default=True, verbose_name='is draft', db_index=True, editable=False),
        ),
        migrations.AddField(
            model_name='urlnode',
            name='publisher_linked',
            field=models.OneToOneField(related_name='publisher_draft', null=True, on_delete=django.db.models.deletion.SET_NULL, editable=False, to='fluent_pages.UrlNode'),
        ),
        migrations.AddField(
            model_name='urlnode',
            name='publisher_modified_at',
            field=models.DateTimeField(default=django.utils.timezone.now, verbose_name='modified at', editable=False),
        ),
    ]
