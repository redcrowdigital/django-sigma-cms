# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('fluent_pages', '0003_urlnode_publisher_published_at'),
    ]

    operations = [
        migrations.AlterField(
            model_name='urlnode',
            name='publisher_published_at',
            field=models.DateTimeField(verbose_name='published at', null=True, editable=False),
        ),
    ]
