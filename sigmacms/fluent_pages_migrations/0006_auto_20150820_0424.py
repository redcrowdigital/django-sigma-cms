# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('fluent_pages', '0005_auto_20150720_0559'),
    ]

    operations = [
        migrations.AlterField(
            model_name='urlnode',
            name='in_navigation',
            field=models.BooleanField(default=False, db_index=True, verbose_name='show in navigation'),
            preserve_default=True,
        ),
    ]
