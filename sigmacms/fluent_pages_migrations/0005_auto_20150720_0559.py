# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('fluent_pages', '0004_auto_20150621_0508'),
    ]

    operations = [
        migrations.AddField(
            model_name='urlnode',
            name='published',
            field=models.BooleanField(default=False, verbose_name='published', editable=False),
            preserve_default=True,
        ),
    ]
