# -*- coding: utf-8 -*-

from django.core.exceptions import MultipleObjectsReturned

from filer.models import Clipboard, tools


class CheckupMiddleware():

    def process_request(self, request):
        if hasattr(request, 'user') and request.user.is_authenticated():
            if request.user.is_staff:
                try:
                    tools.get_user_clipboard(request.user)  # must be one?
                except MultipleObjectsReturned:
                    for index, c in enumerate(Clipboard.objects.filter(user=request.user)):
                        # delete all related clipboards except first one...
                        if index == 0: continue

                        tools.delete_clipboard(c)
                        c.delete()

