# -*- coding: utf-8 -*-

def usersettings(request):
    if hasattr(request, 'usersettings'):
        usersettings = request.usersettings
    else:
        from usersettings.shortcuts import get_current_usersettings

        usersettings = get_current_usersettings()

    return {
        'usersettings': usersettings
    }
