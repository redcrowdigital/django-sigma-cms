# -*- coding: utf-8 -*-

from fluent_pages.extensions import PageTypePlugin, page_type_pool

from .models import ListNode
from .admin import ListNodeAdmin


@page_type_pool.register
class ListNodePlugin(PageTypePlugin):
    model = ListNode
    model_admin = ListNodeAdmin
    sort_priority = 10

    def get_render_template(self, request, node, **kwargs):
        return self.model.render_template or self.render_template
