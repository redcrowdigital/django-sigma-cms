# -*- coding: utf-8 -*-

from django.contrib import admin
from django.template.loader import get_template
from django.utils.translation import ugettext_lazy as _

from fluent_pages.admin import HtmlPageAdmin as PageAdmin

from sigmacms.integration.admin import PageAdminForm


class ListAdminForm(PageAdminForm):
    pass


class ListNodeAdmin(PageAdmin):
    base_form = ListAdminForm

    FIELDSET_GENERAL = (None, {
        'fields': ('title', 'slug', 'in_navigation', 'paginated_by'),
    })

    FIELDSET_SEO = (_('SEO settings'), {
        'fields': (
            'meta_title', 'meta_keywords', 'meta_description', 'in_sitemaps', 'social_bookmarks', 
        ),
        'classes': ('collapse',),
    })

    fieldsets = (
        FIELDSET_GENERAL,
        FIELDSET_SEO,
        PageAdmin.FIELDSET_MENU,
        PageAdmin.FIELDSET_PUBLICATION,
    )

    change_form_template = [
        "admin/fluent_pages/page/change_form.html",
        PageAdmin.base_change_form_template
    ]

    def get_page_template(self, page):
        if getattr(self.model, 'render_template', None):
            return get_template(self.model.render_template)

        return super(ListNodeAdmin, self).get_page_template(page)
