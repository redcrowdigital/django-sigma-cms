# -*- coding: utf-8 -*-

from django.db import models
from django.conf import settings
from django.core.exceptions import ObjectDoesNotExist
from django.utils.translation import ugettext_lazy as _

from parler.models import TranslatedFields

from sigmacms.models import SigmaPageMixin, HtmlPage as Page


class AbstractListNode(SigmaPageMixin, Page):

    render_template = None

    publisher_clone_fields = SigmaPageMixin.publisher_clone_fields + [
        'paginated_by',
        'social_bookmarks',
    ]

    paginated_by = models.IntegerField(_("paginated by"), default=6,
        help_text=_('How many objects should be displayed per page?'))

    social_bookmarks = models.BooleanField(_('social bookmarks'), default=False,
        help_text=_('Enable / disable social media sharing, similar to AddThis'))

    class Meta:
        abstract = True
        verbose_name = _("List")
        verbose_name_plural = _("Lists")

    @property
    def content(self):
        qs = self.get_children().filter(publisher_is_draft=self.is_draft)

        if not self.is_draft:
            qs = qs.filter(published=True)

        return qs.language(language_code=self.get_current_language())


class ListNode(AbstractListNode):
    render_template = 'listnode/default.html'

    class Meta:
        abstract = 'sigmacms.pagetypes.listnode' not in settings.INSTALLED_APPS
