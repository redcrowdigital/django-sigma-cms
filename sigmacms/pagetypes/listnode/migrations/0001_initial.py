# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import sigmacms.models


class Migration(migrations.Migration):

    dependencies = [
        ('fluent_pages', '0007_auto_20150820_0529'),
    ]

    operations = [
        migrations.CreateModel(
            name='ListNode',
            fields=[
                ('urlnode_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='fluent_pages.UrlNode')),
                ('paginated_by', models.IntegerField(default=6, help_text='How many objects should be displayed per page?', verbose_name='paginated by')),
                ('social_bookmarks', models.BooleanField(default=False, help_text='Enable / disable social media sharing, similar to AddThis', verbose_name='social bookmarks')),
            ],
            options={
                'abstract': False,
                'db_table': 'pagetype_listnode_listnode',
                'verbose_name': 'List',
                'verbose_name_plural': 'Lists',
            },
            bases=(sigmacms.models.SigmaPageMixin, 'fluent_pages.htmlpage'),
        ),
    ]
