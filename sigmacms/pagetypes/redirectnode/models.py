# -*- coding: utf-8 -*-

from django.db import models
from django.core.exceptions import ObjectDoesNotExist
from django.utils.translation import ugettext_lazy as _

from parler.models import TranslatedFields

from filer.fields.image import FilerImageField

from fluent_utils.softdeps.any_urlfield import AnyUrlField

from sigmacms.models import SigmaPageMixin, Page


class RedirectNode(SigmaPageMixin, Page):

    REDIRECT_TYPE_CHOICES = (
        (302, _("Normal redirect")),
        (301, _("Permanent redirect (for SEO ranking)")),
    )

    redirect_translations = TranslatedFields(
        new_url = AnyUrlField(_("New URL"), max_length=255),
        redirect_type = models.IntegerField(
            _("Redirect type"), choices=REDIRECT_TYPE_CHOICES, default=302,
            help_text=_("Use 'normal redirect' unless you want to transfer SEO ranking to the new page."),
        ),

        # Text to be used on page teaser plugin(s) and list pages.
        teaser_description = models.TextField(
            _("teaser description"), blank=True, null=True
        ),

        # Image to be used on page teaser plugin(s) and list pages.
        teaser_image = FilerImageField(blank=True, null=True, on_delete=models.SET_NULL),
    )

    class Meta:
        verbose_name = _("Redirect")
        verbose_name_plural = _("Redirects")

    # While it's very tempting to overwrite get_absolute_url() or 'url' with the new URL,
    # the consequences for caching are probably too big to cope with. Just redirect instead.

    @staticmethod
    def clone_translations(src_obj, dst_obj):
        super(RedirectNode, src_obj).clone_translations(src_obj, dst_obj)

        if hasattr(src_obj, 'redirect_translations'):
            for t in src_obj.redirect_translations.all():
                language_code = t.language_code
                try:
                    translation = src_obj.publisher_linked.redirect_translations.get(
                        language_code=language_code)
                except (ObjectDoesNotExist, AttributeError):
                    translation = t
                    translation.pk = None
                    translation.master = dst_obj

                for field, value in t.__dict__.iteritems():
                    if field not in ['id', 'master_id', 'language_code']:
                        setattr(translation, field, value)
                translation.save()

    @staticmethod
    def copy_translations(src_obj, dst_obj):
        super(RedirectNode, src_obj).copy_translations(src_obj, dst_obj)

        if hasattr(src_obj, 'redirect_translations'):
            for t in src_obj.redirect_translations.all():
                translation = t
                translation.pk = None
                translation.master = dst_obj

                for field, value in t.__dict__.iteritems():
                    if field not in ['id', 'master_id', 'language_code']:
                        setattr(translation, field, value)
                translation.save()

    def delete(self):
        self.redirect_translations.all().delete()
        super(RedirectNode, self).delete()
