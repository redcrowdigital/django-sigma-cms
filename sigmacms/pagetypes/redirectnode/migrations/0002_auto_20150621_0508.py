# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('redirectnode', '0001_initial'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='redirectnode',
            options={'verbose_name': 'Redirect', 'verbose_name_plural': 'Redirects', 'permissions': (('can_publish', 'Can publish'),)},
        ),
    ]
