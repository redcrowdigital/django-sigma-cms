# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import filer.fields.image
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('filer', '0002_auto_20150606_2003'),
        ('redirectnode', '0003_auto_20160626_0330'),
    ]

    operations = [
        migrations.AddField(
            model_name='redirectnodetranslation',
            name='teaser_description',
            field=models.TextField(null=True, verbose_name='teaser description', blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='redirectnodetranslation',
            name='teaser_image',
            field=filer.fields.image.FilerImageField(on_delete=django.db.models.deletion.SET_NULL, blank=True, to='filer.Image', null=True),
            preserve_default=True,
        ),
    ]
