# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('redirectnode', '0002_auto_20150621_0508'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='redirectnode',
            options={'verbose_name': 'Redirect', 'verbose_name_plural': 'Redirects'},
        ),
    ]
