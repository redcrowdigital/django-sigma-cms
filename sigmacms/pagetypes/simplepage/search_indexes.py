# -*- coding: utf-8 -*-

import datetime

from django.test import RequestFactory
from django.template.defaultfilters import striptags

from bleach import clean
from haystack import indexes

from fluent_contents.plugins.text.models import TextItem

from .models import SimplePage as Page


class PageIndex(indexes.SearchIndex, indexes.Indexable):

    text = indexes.CharField(document=True, use_template=True)
    date_added = indexes.DateTimeField(model_attr='creation_date')
    title = indexes.CharField(document=False)

    def get_model(self):
        return Page

    def get_updated_field(self):
        return 'publisher_modified_at'
    
    def index_queryset(self, using=None):
        from fluent_pages.models import UrlNode

        qs = UrlNode.objects.published().instance_of(self.get_model())

        return self.get_model().objects.language(
            language_code=using).filter(pk__in=qs.values_list('id', flat=True))

    def read_queryset(self, using=None):
        from fluent_pages.models import UrlNode

        qs = UrlNode.objects.published().instance_of(self.get_model())

        return self.get_model().objects.language(
            language_code=using).filter(pk__in=qs.values_list('id', flat=True))

    def full_prepare(self, obj):
        prepared_data = super(PageIndex, self).full_prepare(obj)

        factory = RequestFactory()

        content = []
        for p in obj.placeholder_set.all():
            for c in p.get_content_items(obj, limit_parent_language=True).non_polymorphic():
                if isinstance(c.get_real_instance(), TextItem):
                    request, plugin, item = factory.get('/'), c.plugin, c.get_real_instance()
                    content.append(striptags(clean(plugin.render(request, item), strip=True)))

        if 'text' in prepared_data:
            prepared_data['text'] += ''.join(content)

        prepared_data['title'] = obj.title

        self.prepared_data = prepared_data
        return self.prepared_data

