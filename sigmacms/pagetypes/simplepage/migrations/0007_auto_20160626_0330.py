# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('simplepage', '0006_auto_20151011_0623'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='simplepage',
            options={'verbose_name': 'Page', 'verbose_name_plural': 'Pages', 'permissions': (('change_page_layout', 'Can change layout'),)},
        ),
    ]
