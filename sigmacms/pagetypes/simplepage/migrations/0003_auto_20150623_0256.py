# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('simplepage', '0002_auto_20150508_0542'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='simplepage',
            options={'verbose_name': 'Page', 'verbose_name_plural': 'Pages', 'permissions': (('change_page_layout', 'Can change layout'), ('can_publish', 'Can publish'))},
        ),
        migrations.AddField(
            model_name='simplepage',
            name='social_links',
            field=models.BooleanField(default=False, help_text='Social media share buttons, similar to AddThis', verbose_name='social links'),
        ),
    ]
