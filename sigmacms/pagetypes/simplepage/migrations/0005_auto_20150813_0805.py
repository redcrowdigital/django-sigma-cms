# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('simplepage', '0004_merge'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='simplepage',
            name='social_links',
        ),
        migrations.AddField(
            model_name='simplepage',
            name='social_bookmarks',
            field=models.BooleanField(default=False, help_text='Enable / disable social media sharing, similar to AddThis', verbose_name='social bookmarks'),
            preserve_default=True,
        ),
    ]
