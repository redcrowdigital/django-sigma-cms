# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import filer.fields.image


class Migration(migrations.Migration):

    dependencies = [
        ('filer', '0002_auto_20150606_2003'),
        ('simplepage', '0005_auto_20150813_0805'),
    ]

    operations = [
        migrations.AddField(
            model_name='simplepage',
            name='teaser_description',
            field=models.TextField(null=True, verbose_name='teaser description', blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='simplepage',
            name='teaser_image',
            field=filer.fields.image.FilerImageField(blank=True, to='filer.Image', null=True),
            preserve_default=True,
        ),
    ]
