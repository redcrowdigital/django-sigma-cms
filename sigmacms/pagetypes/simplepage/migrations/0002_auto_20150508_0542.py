# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('simplepage', '0001_initial'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='simplepage',
            options={'verbose_name': 'Page', 'verbose_name_plural': 'Pages', 'permissions': (('change_page_layout', 'Can change layout'),)},
        ),
        migrations.AlterField(
            model_name='simplepage',
            name='layout',
            field=models.ForeignKey(verbose_name='layout', to='fluent_pages.PageLayout', null=True),
        ),
    ]
