# -*- coding: utf-8 -*-

from django.template.loader import get_template
from django.utils.translation import ugettext_lazy as _

from fluent_pages.pagetypes.fluentpage.admin import FluentPageAdmin

from sigmacms.integration.admin import PageAdminForm


class SimplePageAdminForm(PageAdminForm):

    def __init__(self, *args, **kwargs):
        super(SimplePageAdminForm, self).__init__(*args, **kwargs)
        if 'layout' in self.fields:
            self.fields['layout'].queryset = self.get_layout_queryset(
                self.fields['layout'].queryset)

    def get_layout_queryset(self, base_qs):
        return base_qs


class SimplePageAdmin(FluentPageAdmin):
    base_form = SimplePageAdminForm

    FIELDSET_GENERAL = (None, {
        'fields': ('title', 'slug', 'layout', 'in_navigation'),
    })

    FIELDSET_PROMO = (_('Promotion settings'), {
        'fields': (
            'meta_title', 'meta_keywords', 'meta_description',
            'in_sitemaps', 'social_bookmarks', 'teaser_description', 'teaser_image',
        ),
        'classes': ('collapse',),
    })

    base_fieldsets = (
        FIELDSET_GENERAL,
        FIELDSET_PROMO,
        FluentPageAdmin.FIELDSET_MENU,
        FluentPageAdmin.FIELDSET_PUBLICATION,
    )

    change_form_template = [
        "admin/fluent_pages/page/change_form.html",
        FluentPageAdmin.base_change_form_template
    ]

    def get_page_template(self, page):
        if getattr(self.model, 'render_template', None):
            return get_template(self.model.render_template)

        return super(SimplePageAdmin, self).get_page_template(page)
