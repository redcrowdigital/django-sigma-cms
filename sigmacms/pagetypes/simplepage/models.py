# -*- coding: utf-8 -*-

from django.db import models
from django.conf import settings
from django.core.exceptions import ObjectDoesNotExist
from django.utils.translation import ugettext_lazy as _

from filer.fields.image import FilerImageField

from sigmacms.models import SigmaPageMixin, FluentContentsPage


class AbstractSimplePage(SigmaPageMixin, FluentContentsPage):

    render_template = None

    publisher_clone_fields = SigmaPageMixin.publisher_clone_fields + [
        'layout',
        'social_bookmarks',
        'teaser_description',
        'teaser_image',
    ]

    layout = models.ForeignKey('fluent_pages.PageLayout', verbose_name=_('layout'), null=True)

    social_bookmarks = models.BooleanField(_('social bookmarks'), default=False,
        help_text=_('Enable / disable social media sharing, similar to AddThis'))

    # Text to be used on page teaser plugin(s) and list pages.
    teaser_description = models.TextField(
        _("teaser description"), blank=True, null=True
    )

    # Image to be used on page teaser plugin(s) and list pages.
    teaser_image = FilerImageField(blank=True, null=True, on_delete=models.SET_NULL)

    class Meta:
        abstract = True
        verbose_name = _("Page")
        verbose_name_plural = _("Pages")
        permissions = (
            ('change_page_layout', _("Can change layout")),
        )


class SimplePage(AbstractSimplePage):

    class Meta:
        abstract = 'sigmacms.pagetypes.simplepage' not in settings.INSTALLED_APPS
