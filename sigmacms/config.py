import os
from datetime import timedelta

from django.utils import timezone
from django.db.models import SET_NULL
from django.conf import global_settings as settings

from . import __version__ as VERSION

def setup(conf):

    conf['EXTRA_MODEL_FIELDS'] = (
        (
            "fluent_pages.models.db.UrlNode.publisher_linked",
            "OneToOneField", # 'django.db.models.' is implied if path is omitted.
            ("self",),
            dict(related_name='publisher_draft', null=True, editable=False, on_delete=SET_NULL),
        ),
        (
            "fluent_pages.models.db.UrlNode.published",
            "BooleanField", # 'django.db.models.' is implied if path is omitted.
            (u"published",),
            dict(default=False, editable=False),
        ),
        (
            "fluent_pages.models.db.UrlNode.publisher_is_draft",
            "BooleanField", # 'django.db.models.' is implied if path is omitted.
            (u"is draft",),
            dict(default=True, editable=False, db_index=True),
        ),
        (
            "fluent_pages.models.db.UrlNode.publisher_modified_at",
            "DateTimeField", # 'django.db.models.' is implied if path is omitted.
            (u"modified at",),
            dict(default=timezone.now, editable=False),
        ),
        (
            "fluent_pages.models.db.UrlNode.publisher_published_at",
            "DateTimeField", # 'django.db.models.' is implied if path is omitted.
            (u"published at",),
            dict(null=True, editable=False),
        ),
    )

    conf['INSTALLED_APPS'] = conf.get('INSTALLED_APPS', []) + [
        'sigmacms.bootstrap',
        'usersettings',

        'axes',
        'loginas',
        'logentry_admin',

        'adminplus',
        'admin_enhancer',
        'django_wysiwyg',
        'easy_select2',
        'easy_maps',

        'fluent_pages',
        'publisher',

        'any_imagefield',
        'any_urlfield',

        'mptt',
        'parler',
        'polymorphic',
        'polymorphic_tree',

        'filer',
        'easy_thumbnails',

        'fluent_contents',
        'fluent_contents.plugins.text',
        'fluent_contents.plugins.sharedcontent',

        'django.contrib.admin.apps.SimpleAdminConfig',

        'sigmacms.tests',
    ]

    if 'filer' in conf['INSTALLED_APPS']:
        conf['INSTALLED_APPS'] += [
            'sigmacms.contrib.filerpicker',
        ]

    if 'suit' in conf['INSTALLED_APPS']:
        idx = conf['INSTALLED_APPS'].index('suit') - 1
        conf['INSTALLED_APPS'].insert(
            idx, 'sigmacms.contrib.fluent_suit')
    if 'SUIT_CONFIG' in conf and 'ADMIN_NAME' in conf['SUIT_CONFIG']:
        if conf['SUIT_CONFIG']['ADMIN_NAME'] == 'Sigma CMS':
            conf['SUIT_CONFIG']['VERSION'] = VERSION

    if 'haystack' in conf['INSTALLED_APPS']:
        conf['INSTALLED_APPS'] += [
            'sigmacms.contrib.simplesearch',
        ]

    if 'post_office' in conf['INSTALLED_APPS']:
        conf['INSTALLED_APPS'] += [
            'sigmacms.contrib.mail',
        ]

    if 'dbbackup' in conf['INSTALLED_APPS']:
        conf['INSTALLED_APPS'] += [
            'sigmacms.contrib.backup',
        ]

    if 'django.contrib.admin' in conf['INSTALLED_APPS']:
        conf['INSTALLED_APPS'].remove('django.contrib.admin')

    conf['TEMPLATE_CONTEXT_PROCESSORS'] = list(conf.get(
        'TEMPLATE_CONTEXT_PROCESSORS', settings.TEMPLATE_CONTEXT_PROCESSORS)) + [
            'sigmacms.context_processors.usersettings',
    ]

    if not 'django.core.context_processors.request' in conf['TEMPLATE_CONTEXT_PROCESSORS']:
        conf['TEMPLATE_CONTEXT_PROCESSORS'] += [
            'django.core.context_processors.request',
        ]

    conf['MIDDLEWARE_CLASSES'] = list(conf.get('MIDDLEWARE_CLASSES', settings.MIDDLEWARE_CLASSES)) + [
        'usersettings.middleware.CurrentUserSettingsMiddleware',
        'publisher.middleware.PublisherMiddleware',
        'sigmacms.middleware.CheckupMiddleware',
    ]

    if not 'django.middleware.locale.LocaleMiddleware' in conf['MIDDLEWARE_CLASSES']:
        conf['MIDDLEWARE_CLASSES'] += [
            'django.middleware.locale.LocaleMiddleware',
        ]

    conf['MIGRATION_MODULES'] = conf.get('MIGRATION_MODULES', {})
    conf['MIGRATION_MODULES'].update({
        'fluent_pages': 'sigmacms.fluent_pages_migrations',
    })

    conf['SITE_NAME'] = conf.get('SITE_NAME', None)
    if conf['SITE_NAME'] is None:
        conf['SITE_NAME'] = 'Sigma CMS'


    ## Loginas settings

    conf['CAN_LOGIN_AS'] = lambda r, u: r.user.is_superuser and not (u.is_superuser or u.is_editor or u.is_publisher)


    ## Axes settings

    conf['AXES_COOLOFF_TIME'] = conf.get('AXES_COOLOFF_TIME', None)
    if conf['AXES_COOLOFF_TIME'] is None:
        conf['AXES_COOLOFF_TIME'] = timedelta(minutes=15)


    ## Export settings

    if 'export' in conf['INSTALLED_APPS']:
        conf['SERIALIZATION_MODULES'] = conf.get('SERIALIZATION_MODULES', {})
        if not 'csv' in conf['SERIALIZATION_MODULES']:
            conf['SERIALIZATION_MODULES'].update({
                'csv': 'export.serializers.csv_serializer',
            })


    ## Filer settings

    conf['FILER_ALLOW_REGULAR_USERS_TO_ADD_ROOT_FOLDERS'] = True


    ## Parler settings

    conf['PARLER_DEFAULT_LANGUAGE'] = 'en'
    conf['PARLER_LANGUAGES'] = {
        conf['SITE_ID']: tuple([{'code': lang[0]} for lang in conf.get('LANGUAGES', conf['LANGUAGE_CODE'])]),
        'default': {
            'fallback': conf['LANGUAGE_CODE'],
            'hide_untranslated': False,
        }
    }


    ## DBBackup settings

    if 'dbbackup' in conf['INSTALLED_APPS']:
        conf['DBBACKUP_STORAGE'] = 'sigmacms.contrib.backup.storage'


    ## Fluent CMS settings

    conf['FLUENT_TEXT_CLEAN_HTML'] = False
    conf['FLUENT_TEXT_SANITIZE_HTML'] = False

    conf['FLUENT_PAGES_PARENT_ADMIN_MIXIN'] = 'sigmacms.integration.admin.SigmaAdminMixin'
    conf['FLUENT_PAGES_CHILD_ADMIN_MIXIN'] = 'sigmacms.integration.admin.SigmaAdminMixin'

    ## WYSIWYG settings

    conf['DJANGO_WYSIWYG_FLAVOR'] = "ckeditor"


    ## Misc. settings

    conf['USERSETTINGS_MODEL'] = conf.get('USERSETTINGS_MODEL', None)
    if conf['USERSETTINGS_MODEL'] is None:
        conf['INSTALLED_APPS'] += [
            'sigmacms.contrib.sitesettings',
        ]
        conf['USERSETTINGS_MODEL'] = 'sitesettings.SiteSettings'
