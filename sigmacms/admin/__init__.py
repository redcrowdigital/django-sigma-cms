# -*- coding: utf-8 -*-

from django import forms
from django.conf import settings
from django.contrib.auth import get_user_model
from django.core.exceptions import PermissionDenied
from django.contrib.admin.sites import NotRegistered
from django.http import Http404, HttpResponseRedirect
from django.contrib.contenttypes.models import ContentType
from django.contrib.admin.helpers import AdminErrorList, AdminForm
from django.utils.translation import ugettext as _, ugettext_lazy
from django.utils.safestring import mark_safe
from django.utils.encoding import force_text

from publisher.admin import http_json_response

from usersettings.admin import SettingsAdmin
from usersettings.shortcuts import get_usersettings_model

try:
    from authtools.admin import NamedUserAdmin as UserAdmin
except ImportError:
    from django.contrib.auth.admin import UserAdmin

from ..bootstrap import LazyAdminSite as AdminSite, admin

from .adminforms import CustomUserCreationForm, CustomUserChangeForm

site = admin.site
site.index_title = ugettext_lazy('Dashboard')


class SiteSettingsAdmin(SettingsAdmin):

    REQUIRED_FIELDS = ('site_name', 'contact_email')

    BASE_FIELDS = (None, {
        'fields': REQUIRED_FIELDS + ('favicon', 'send_mail'),
    })

    DBBACKUP_FIELDS = (_('Backup settings'), {
        'fields': (
            'dbbackup_enabled', 'mediabackup_enabled',
            'access_key', 'secret_key', 'bucket_name',
        ),
    })

    STYLE_FIELDS = (_('Style settings'), {
        'fields': (
            'logo', 'bg_color', 'fg_color'
        )
    })

    ANALYTICS_FIELDS = (_('Google settings'), {
        'fields': (
            'keycode', 'client_secrets', 'analytics_profile_id', 'maps_api_key'
        )
    })

    fieldsets = (
        BASE_FIELDS,
        STYLE_FIELDS
    )

    if 'django_cron' in settings.INSTALLED_APPS:
        DBBACKUP_FIELDS[1]['fields'] = DBBACKUP_FIELDS[1]['fields'] + ('backup_frequency',)

    if 'dbbackup' in settings.INSTALLED_APPS:
        fieldsets += (
            DBBACKUP_FIELDS,
        )

    if 'analytics_tools' in settings.INSTALLED_APPS:
        fieldsets += (
            ANALYTICS_FIELDS,
        )

if 'sigmacms.contrib.sitesettings' in settings.INSTALLED_APPS:
    site.register(get_usersettings_model(), SiteSettingsAdmin)


class CustomUserAdmin(UserAdmin):
    USERNAME_FIELD = get_user_model().USERNAME_FIELD

    REQUIRED_FIELDS = (USERNAME_FIELD,) + tuple(get_user_model().REQUIRED_FIELDS)

    BASE_FIELDS = (None, {
        'fields': REQUIRED_FIELDS + ('password',),
    })

    SIMPLE_PERMISSION_FIELDS = (_('Permissions'), {
        'fields': ('is_active', 'is_staff', 'is_editor', 'is_publisher', 'is_superuser',),
    })

    DATE_FIELDS = (_('Important dates'), {
        'fields': ('last_login', 'date_joined',),
    })

    list_display = ('is_active', USERNAME_FIELD, 'is_editor', 'is_publisher', 'is_superuser',)
    list_display_links = (USERNAME_FIELD,)

    add_form = CustomUserCreationForm
    form = CustomUserChangeForm
    change_form_template = 'loginas/change_form.html'

    fieldsets = (
        BASE_FIELDS,
        SIMPLE_PERMISSION_FIELDS,
        DATE_FIELDS,
    )

    def is_editor(self, obj):
        return obj.is_editor
    is_editor.short_description = _("editor status")
    is_editor.boolean = True

    def is_publisher(self, obj):
        return obj.is_publisher
    is_publisher.short_description = _("publisher status")
    is_publisher.boolean = True

    def save_model(self, request, obj, form, change):
        super(CustomUserAdmin, self).save_model(request, obj, form, change)

        if form.cleaned_data.get('is_publisher', False):
            obj.is_publisher = True
        else:
            obj.is_publisher = False

        if form.cleaned_data.get('is_editor', False):
            obj.is_editor = True
        else:
            obj.is_editor = False

        obj.save()


if settings.AUTH_USER_MODEL in ('authtools.User', 'auth.User'):
    try:
        site.unregister(get_user_model())
    except NotRegistered:
        pass
    site.register(get_user_model(), CustomUserAdmin)


if 'fluent_pages' in settings.INSTALLED_APPS:
    from fluent_pages.models import Page, PageLayout
    from fluent_pages.admin import PageLayoutAdmin, PageParentAdmin

    from .mixins import StaffAdminMixin

    from django.conf.urls import patterns, url
    from django.core.exceptions import PermissionDenied
    from django.contrib import messages
    from django.http import HttpResponseRedirect
    from django.core.urlresolvers import reverse

    class PageParentAdmin(StaffAdminMixin, PageParentAdmin):
        from parler.utils import is_multilingual_project
        from parler import appsettings as parler_appsettings

        if is_multilingual_project() and len(parler_appsettings.PARLER_LANGUAGES.get(settings.SITE_ID)) > 1:
            list_display = (
                'publisher_object_title', 'language_column', 'status_column', 'pagetype_column', 'get_layout', 'modification_date',
                'actions_column')
        else:
            list_display = (
                'publisher_object_title', 'status_column', 'pagetype_column', 'get_layout', 'modification_date', 'actions_column')

        def get_layout(self, obj):
            if hasattr(obj, 'layout'):
                return obj.layout
            else:
                return '-'
        get_layout.short_description = 'Layout'

        def __init__(self, model, admin_site):
            super(PageParentAdmin, self).__init__(model, admin_site)
            self.request = None
            self.clone_reverse = '%s:%sclone' % (
                self.admin_site.name,
                self.url_name_prefix,)

        def get_urls(self):
            urls = super(PageParentAdmin, self).get_urls()
            clone_name = '%sclone' % (self.url_name_prefix,)
            clone_urls = patterns('',
                                  url(r'^(?P<object_id>\d+)/clone/$',
                                      self.clone_view, name=clone_name),
                                  )
            return clone_urls + urls

        def clone_view(self, request, object_id):
            obj = self.get_model_object(request, object_id)

            if not self.has_add_permission(request):
                raise PermissionDenied

            obj.clone_page()

            if not request.is_ajax():
                messages.success(request, _('Clone of the page was successfully created.'))
                return HttpResponseRedirect(reverse(self.changelist_reverse))

            return http_json_response({'success': True})

        def add_type_view(self, request, form_url=''):
            from etc.toolbox import get_model_class_from_settings

            if not self.has_add_permission(request):
                raise PermissionDenied

            extra_qs = ''
            if request.META['QUERY_STRING']:
                extra_qs = '&' + request.META['QUERY_STRING']

            choices = self.get_child_type_choices(request, 'add')
            if len(choices) == 1:
                return HttpResponseRedirect('?ct_id={0}{1}'.format(choices[0][0], extra_qs))

            try:
                ct = ContentType.objects.get_for_model(
                    get_model_class_from_settings(settings, 'FLUENT_PAGES_DEFAULT_PAGETYPE'),
                    for_concrete_model=False,
                )
                if ct.pk in [k for k,v in choices]:
                    initial = {'ct_id': ct.pk}
                # reordering choices...
                choices.insert(0, choices.pop([k for k,v in enumerate(choices) if v[0] == initial['ct_id']][0]))
            except:
                initial = {'ct_id': choices[0][0]}

            # Create form
            form = self.add_type_form(
                data=request.POST if request.method == 'POST' else None,
                initial=initial,
            )
            form.fields['ct_id'].choices = choices

            if form.is_valid():
                return HttpResponseRedirect('?ct_id={0}{1}'.format(form.cleaned_data['ct_id'], extra_qs))

            # Wrap in all admin layout
            fieldsets = ((None, {'fields': ('ct_id',)}),)
            adminForm = AdminForm(form, fieldsets, {}, model_admin=self)
            media = self.media + adminForm.media
            opts = self.model._meta

            context = {
                'title': _('Add %s') % force_text(opts.verbose_name),
                'adminform': adminForm,
                'is_popup': ("_popup" in request.POST or
                             "_popup" in request.GET),
                'media': mark_safe(media),
                'errors': AdminErrorList(form, ()),
                'app_label': opts.app_label,
            }
            return self.render_add_type_form(request, context, form_url)

    class PageLayoutAdmin(StaffAdminMixin, PageLayoutAdmin):
        pass

    try:
        site.unregister(Page)
    except NotRegistered:
        pass
    site.register(Page, admin_class=PageParentAdmin)

    try:
        site.unregister(PageLayout)
    except NotRegistered:
        pass
    site.register(PageLayout, admin_class=PageLayoutAdmin)


if 'filer' in settings.INSTALLED_APPS:
    from filer.admin.clipboardadmin import ClipboardAdmin
    from filer.admin.fileadmin import FileAdmin
    from filer.admin.folderadmin import FolderAdmin
    from filer.admin.imageadmin import ImageAdmin
    from filer.admin.permissionadmin import PermissionAdmin
    from filer.models import FolderPermission, Folder, File, Clipboard, Image

    from .mixins import StaffAdminMixin

    class FolderAdmin(StaffAdminMixin, FolderAdmin):

        def has_add_permission(self, request):
            return False

    class FileAdmin(StaffAdminMixin, FileAdmin):

        def has_add_permission(self, request):
            return False

    class ImageAdmin(StaffAdminMixin, ImageAdmin):

        def has_add_permission(self, request):
            return False

    class ClipboardAdmin(StaffAdminMixin, ClipboardAdmin):
        pass

    class PermissionAdmin(StaffAdminMixin, PermissionAdmin):
        pass

    try:
        site.unregister(Folder)
    except NotRegistered:
        pass
    site.register(Folder, admin_class=FolderAdmin)

    try:
        site.unregister(File)
    except NotRegistered:
        pass
    site.register(File, admin_class=FileAdmin)

    try:
        site.unregister(Image)
    except NotRegistered:
        pass
    site.register(Image, admin_class=ImageAdmin)

    try:
        site.unregister(Clipboard)
    except NotRegistered:
        pass
    site.register(Clipboard, admin_class=ClipboardAdmin)

    try:
        site.unregister(FolderPermission)
    except NotRegistered:
        pass
    site.register(FolderPermission, admin_class=PermissionAdmin)


if 'fluentcms_forms_builder' in settings.INSTALLED_APPS:
    from fluentcms_forms_builder.models import Form
    from fluentcms_forms_builder.admin import FormAdmin

    from sigmacms.integration.admin import SuitAdminMixin

    class FormAdminForm(forms.ModelForm):

        class Meta:
            model = Form
            if 'suit' in settings.INSTALLED_APPS:
                from suit.widgets import AutosizedTextarea
                widgets = {
                    'intro': AutosizedTextarea,
                    'response': AutosizedTextarea,
                    'email_message': AutosizedTextarea,
                }
            exclude = []

    class FormAdmin(SuitAdminMixin, StaffAdminMixin, FormAdmin):
        list_display = ("title", "status", "email_copies", "publish_date", "expiry_date", "total_entries")
        change_form_template = "admin/forms/change_form.html"
        list_editable = []
        form = FormAdminForm

    try:
        site.unregister(Form)
    except NotRegistered:
        pass
    site.register(Form, admin_class=FormAdmin)
