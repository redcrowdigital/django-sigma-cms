# -*- coding: utf-8 -*-

from django.conf import settings
from django.db import router, models
from django.contrib import messages
from django.contrib.admin import helpers
from django.core.exceptions import PermissionDenied
from django.contrib.admin.utils import get_deleted_objects, model_ngettext
from django.utils.translation import ugettext_lazy, ugettext as _
from django.template.response import TemplateResponse
from django.utils.encoding import force_text

from easy_select2.widgets import Select2
from admin_enhancer import admin as enhanced_admin


class StaffAdminMixin(object):
    actions = ['delete_selected']

    def has_add_permission(self, request):
        return request.user.is_active and request.user.is_editor

    def has_change_permission(self, request, obj=None):
        return request.user.is_active and request.user.is_editor

    def has_delete_permission(self, request, obj=None):
        return request.user.is_active and request.user.is_editor

    def has_module_permission(self, request):
        return request.user.is_active and request.user.is_editor

    def has_change_shared_fields_permission(self, request, obj=None):
        return request.user.is_active and request.user.is_editor

    def has_change_override_url_permission(self, request, obj=None):
        return request.user.is_active and request.user.is_editor

    def delete_selected(self, request, queryset):
        opts = self.model._meta
        app_label = opts.app_label

        # Check that the user has delete permission for the actual model
        if not self.has_delete_permission(request):
            raise PermissionDenied

        using = router.db_for_write(self.model)

        # Populate deletable_objects, a data structure of all related objects that
        # will also be deleted.
        deletable_objects, perms_needed, protected = get_deleted_objects(
            queryset, opts, request.user, self.admin_site, using)

        # The user has already confirmed the deletion.
        # Do the deletion and return a None to display the change list view again.
        if request.POST.get('post'):
            if perms_needed:
                raise PermissionDenied
            n = queryset.count()
            if n:
                for obj in queryset:
                    obj_display = force_text(obj)
                    self.log_deletion(request, obj, obj_display)
                    obj.delete()
                self.message_user(request, _("Successfully deleted %(count)d %(items)s.") % {
                    "count": n, "items": model_ngettext(self.opts, n)
                }, messages.SUCCESS)
            # Return None to display the change list page again.
            return None

        if len(queryset) == 1:
            objects_name = force_text(opts.verbose_name)
        else:
            objects_name = force_text(opts.verbose_name_plural)

        if perms_needed or protected:
            title = _("Cannot delete %(name)s") % {"name": objects_name}
        else:
            title = _("Are you sure?")

        context = dict(
            self.admin_site.each_context(),
            title=title,
            objects_name=objects_name,
            deletable_objects=[deletable_objects],
            queryset=queryset,
            perms_lacking=perms_needed,
            protected=protected,
            opts=opts,
            action_checkbox_name=helpers.ACTION_CHECKBOX_NAME,
        )

        # Display the confirmation page
        return TemplateResponse(request, self.delete_selected_confirmation_template or [
            "admin/%s/%s/delete_selected_confirmation.html" % (app_label, opts.model_name),
            "admin/%s/delete_selected_confirmation.html" % app_label,
            "admin/delete_selected_confirmation.html"
        ], context, current_app=self.admin_site.name)

    delete_selected.short_description = ugettext_lazy("Delete selected %(verbose_name_plural)s")


class ForeignKeyAdminMixin(enhanced_admin.EnhancedModelAdminMixin):
    def formfield_for_dbfield(self, db_field, **kwargs):
        if 'easy_select2' in settings.INSTALLED_APPS:

            if isinstance(db_field, models.ForeignKey):
                kwargs.setdefault('widget', Select2)

        return super(ForeignKeyAdminMixin, self).formfield_for_dbfield(db_field, **kwargs)
