from django.contrib.auth import get_user_model
from django.contrib.auth.models import Group
from django.test import TestCase

from sigmacms.admin import CustomUserChangeForm


User = get_user_model()


class CustomUserChangeFormTestCase(TestCase):
    def test_clean_groups(self):
        editor_group = Group.objects.create(name="Editors")
        publisher_group = Group.objects.create(name="Publishers")
        groups = list(Group.objects.filter(id__in=[editor_group.id, publisher_group.id]))
        user = User.objects.create(username='test', password='test')

        # Ensure if enabled the editor / publisher group gets added
        form = CustomUserChangeForm(
            instance=user,
            data={
                'is_editor': True,
                'is_publisher': True,
                'groups': [],
                'password': '',
                'username': '',
            }
        )
        form.full_clean()
        self.assertEqual(list(form.clean_groups()), groups)
        # Ensure if enabled and groups already present it stays added
        form = CustomUserChangeForm(
            instance=user,
            data={
                'is_editor': True,
                'is_publisher': True,
                'groups': [editor_group.id, publisher_group.id],
                'password': '',
                'username': '',
            }
        )
        form.full_clean()
        self.assertEqual(list(form.clean_groups()), groups)
        # Ensure if disabled the editor / publisher group gets removed
        form = CustomUserChangeForm(
            instance=user,
            data={
                'is_editor': False,
                'is_publisher': False,
                'groups': [editor_group.id, publisher_group.id],
                'password': '',
                'username': '',
            }
        )
        form.full_clean()
        self.assertQuerysetEqual(form.clean_groups(), Group.objects.none())
        # Ensure if disabled and groups not present they stay no present
        form = CustomUserChangeForm(
            instance=user,
            data={
                'is_editor': False,
                'is_publisher': False,
                'groups': [],
                'password': '',
                'username': '',
            }
        )
        form.full_clean()
        self.assertQuerysetEqual(form.clean_groups(), Group.objects.none())
