# -*- coding: utf-8 -*-

from django import forms
from django.contrib.auth.models import Group
from django.utils.translation import ugettext_lazy as _

try:
    from authtools.forms import AdminUserChangeForm as UserChangeForm
except ImportError:
    from django.contrib.auth.forms import UserChangeForm

try:
    from authtools.forms import UserCreationForm
except ImportError:
    from django.contrib.auth.forms import UserCreationForm


class CustomUserCreationForm(UserCreationForm):

    def clean_email(self):
        username = self.cleaned_data.get('email')
        if username:
            username = username.lower()

        return username


class CustomUserChangeForm(UserChangeForm):

    is_editor = forms.BooleanField(
        label=_("Editor status"),
        required=False,
        help_text=_("Designates whether this user should be treated as editor.")
    )

    is_publisher = forms.BooleanField(
        label=_("Publisher status"),
        required=False,
        help_text=_("Designates whether this user should be treated as publisher.")
    )

    def __init__(self, *args, **kwargs):
        super(CustomUserChangeForm, self).__init__(*args, **kwargs)
        self.fields['is_editor'].initial = self.instance.is_editor
        self.fields['is_publisher'].initial = self.instance.is_publisher

    def clean_groups(self):
        """
        Ensure groups are kept in sync with publishing properties.

        Ensure properties that are set in
        `sigmacms.apps.handle_user_model` are respected via the admin
        change form. We automatically set the group the user is allowed
        in to represent what occurs in the property. So if the user
        set the property we need the form to respect that action and
        ensure that the groups added are kept in the group listing.
        :return:
        """
        cleaned_data = self.cleaned_data['groups']
        group_ids = list(cleaned_data.values_list('id', flat=True))

        editor_group_id = Group.objects.get(name="Editors").id
        publisher_group_id = Group.objects.get(name="Publishers").id

        if self.cleaned_data.get('is_editor'):
            group_ids.append(editor_group_id)
        elif editor_group_id in group_ids:
            group_ids.remove(editor_group_id)

        if self.cleaned_data.get('is_publisher'):
            group_ids.append(publisher_group_id)
        elif publisher_group_id in group_ids:
            group_ids.remove(publisher_group_id)

        return Group.objects.filter(id__in=group_ids)
