# -*- coding: utf-8 -*-

from dbbackup.storage.base import StorageError
from dbbackup.storage.builtin_django import Storage as DjangoStorage

from usersettings.shortcuts import get_current_usersettings

STORAGE_PATH = 'storages.backends.s3boto.S3BotoStorage'


class Storage(DjangoStorage):
    name = 'AmazonS3'

    def __init__(self, server_name=None, **options):
        usersettings = get_current_usersettings()
        self._check_filesystem_errors(usersettings)

        super(Storage, self).__init__(
            storage_path=STORAGE_PATH,
            bucket=usersettings.bucket_name,
            access_key=usersettings.access_key,
            secret_key=usersettings.secret_key,
            **options
        )

    def _check_filesystem_errors(self, options):
        for arg in ('bucket_name', 'access_key', 'secret_key'):
            if not getattr(options, arg, None):
                raise StorageError(
                    "%s storage requires settings '%s' to be defined" % (self.name, arg),
                )
