# -*- coding: utf-8 -*-

from optparse import make_option

from django.core.management.base import BaseCommand

from dbbackup.management.commands.dbbackup import Command as DBBackupCommand
from dbbackup.management.commands.mediabackup import Command as MediaBackupCommand

from usersettings.shortcuts import get_current_usersettings


class Command(BaseCommand):
    option_list = BaseCommand.option_list + (
    )

    def handle(self, *args, **options):
        self.verbose = int(options.get('verbosity', 0))

        usersettings = get_current_usersettings()

        if getattr(usersettings, 'dbbackup_enabled', False):
            DBBackupCommand().handle(*args, **options)
        else:
            if self.verbose >= 1:
                self.stdout.write('Database backups are currently disabled in user settings.')            
            
        if getattr(usersettings, 'mediabackup_enabled', False):
            MediaBackupCommand().handle(*args, **options)
        else:
            if self.verbose >= 1:
                self.stdout.write('Media files backups are currently disabled in user settings.')            
