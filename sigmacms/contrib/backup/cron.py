# -*- coding: utf-8 -*-

from django_cron import cronScheduler, Job, HOUR, DAY, WEEK, MONTH

from django.core.management import call_command

from usersettings.shortcuts import get_current_usersettings

class Klass(Job):

    def job(self):
        call_command('backupit', verbosity=0)

    @property
    def run_every(self):
        u = get_current_usersettings()

        try:
            return {
                u.BackupFrequency.daily: DAY,
                u.BackupFrequency.weekly: WEEK,
                u.BackupFrequency.monthly: MONTH,
            }.get(u.backup_frequency, DAY)
        except AttributeError:
            return DAY

cronScheduler.register(Klass)
