from django import template
from easy_thumbnails.files import get_thumbnailer
from usersettings.shortcuts import get_current_usersettings

register = template.Library()


@register.assignment_tag(takes_context=True)
def get_admin_custom_style(context):
    usersettings = get_current_usersettings()

    return {
        'bg_color': getattr(usersettings, 'bg_color', None),
        'fg_color': getattr(usersettings, 'fg_color', None),
    }

@register.assignment_tag(takes_context=True)
def get_admin_logo(context):
    usersettings = get_current_usersettings()
    result = None
    if getattr(usersettings, 'logo', None) is not None:
        try:
            t = get_thumbnailer(usersettings.logo).get_thumbnail(
                {'size': (150, 50), 'crop': True})
            result = t.url
        except:
            pass
    return result
