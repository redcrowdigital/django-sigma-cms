# -*- coding: utf-8 -*-

from optparse import make_option

from django.core.management.base import BaseCommand

from post_office.logutils import setup_loghandlers
from post_office.management.commands.send_queued_mail import Command as PostCommand

from usersettings.shortcuts import get_current_usersettings


class Command(BaseCommand):
    option_list = BaseCommand.option_list + (
        make_option('-p', '--processes', type='int',
                    help='Number of processes used to send emails', default=1),
        make_option('-L', '--lockfile', type='string',
                    help='Absolute path of lockfile to acquire'),
        make_option('-l', '--log-level', type='int',
                    help='"0" to log nothing, "1" to only log errors'),
    )

    def handle(self, *args, **options):
        self.verbose = int(options.get('verbosity', 0))

        usersettings = get_current_usersettings()

        if getattr(usersettings, 'send_mail', False):
            PostCommand().handle(*args, **options)
        else:
            if self.verbose >= 1:
                self.stdout.write('Email sending is currently disabled in user settings.')
