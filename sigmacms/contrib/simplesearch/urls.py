from django.conf.urls import patterns, url

from .views import SearchView
from .forms import SearchForm

urlpatterns = patterns('',
    url(r'^$', SearchView(form_class=SearchForm), name='haystack_search'),
)
