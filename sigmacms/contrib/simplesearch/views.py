# -*- coding: utf-8 -*-

from django.shortcuts import render_to_response

from haystack import views as haystack_views


class SearchView(haystack_views.SearchView):

    def create_response(self):
        (paginator, page) = self.build_page()
        get_params = self.request.GET.copy()
        get_params.pop('page', None)

        context = {
            'query': self.query,
            'form': self.form,
            'page_obj': page,
            'paginator': paginator,
            'suggestion': None,
            'get_params': get_params.urlencode(),
        }

        if self.results and hasattr(self.results, 'query') and self.results.query.backend.include_spelling:
            context['suggestion'] = self.form.get_suggestion()

        context.update(self.extra_context())
        return render_to_response(self.template, context, context_instance=self.context_class(self.request))

    def build_form(self, form_kwargs=None):
        form_kwargs = {}

        ordering = self.request.GET.get('ordering', None)
        if ordering:
            form_kwargs['ordering'] = ordering

        return super(SearchView, self).build_form(form_kwargs)