# -*- coding: utf-8 -*-

from haystack.forms import ModelSearchForm

from django.utils import translation


class SearchForm(ModelSearchForm):
    ordering_fields = ['title', '-title', 'date_added', '-date_added']

    def __init__(self, *args, **kwargs):
        ordering = kwargs.pop('ordering', None)
        super(SearchForm, self).__init__(*args, **kwargs)
        self.using = translation.get_language()

        if ordering:
            self.ordering = ordering

        ordering_fields = kwargs.pop('ordering_fields', None)
        if ordering_fields:
            self.ordering_fields = ordering_fields

    def search(self):
        if not self.is_valid():
            return self.no_query_found()

        if not self.cleaned_data.get('q'):
            return self.no_query_found()

        sqs = self.searchqueryset.using(self.using).auto_query(self.cleaned_data['q'])

        if self.load_all:
            sqs = sqs.load_all()

        if hasattr(self, 'ordering') and self.ordering in self.ordering_fields:
            sqs = sqs.order_by(self.ordering)

        return sqs.models(*self.get_models())
