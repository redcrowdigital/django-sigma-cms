# -*- coding: utf-8 -*-

from django.utils.translation import ugettext_lazy as _

from .abstract_models import AbstractSiteSettings


class SiteSettings(AbstractSiteSettings):

    class Meta:
        abstract = False
        verbose_name = _("Settings")
        verbose_name_plural = _("Settings")

