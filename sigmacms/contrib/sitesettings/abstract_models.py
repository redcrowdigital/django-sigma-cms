# -*- coding: utf-8 -*-
import os

from django.db import models
from django.conf import settings
from django.utils.translation import ugettext_lazy as _
from django.core.files.storage import FileSystemStorage

from djchoices import DjangoChoices, ChoiceItem
from any_imagefield.models import AnyImageField

from usersettings.models import UserSettings


class AbstractSiteSettings(UserSettings):

    class BackupFrequency(DjangoChoices):
        daily = ChoiceItem("d")
        weekly = ChoiceItem("w")
        monthly = ChoiceItem("m")

    site_name = models.CharField(_("Site Name"), max_length=100)
    contact_email = models.CharField(_("Contact Email"), max_length=128)

    favicon = AnyImageField(verbose_name=_('favicon'), upload_to='favicons', blank=True)

    keycode = models.CharField(_("Google Analytics Keycode"), max_length=80, blank=True)
    send_mail = models.BooleanField(_("Send Emails?"), default=False,
        help_text=_("Enable / disable sending of emails by the site."))

    # backup settings...

    dbbackup_enabled = models.BooleanField(_("Backup Database?"), default=False,
        help_text=_("Enable / disable database backups."))
    mediabackup_enabled = models.BooleanField(_("Backup Media Files?"), default=False,
        help_text=_("Enable / disable media files backups."))

    backup_frequency = models.CharField(
        _("Backup Frequency"), max_length=1,
        choices=BackupFrequency.choices, default=BackupFrequency.daily,
        validators=[BackupFrequency.validator],
    )

    # AWS S3 settings...

    access_key = models.CharField(_("Amazon S3 Access Key"), max_length=128, blank=True)
    secret_key = models.CharField(_("Amazon S3 Secret Key"), max_length=128, blank=True)
    bucket_name = models.CharField(_("Amazon S3 Bucket Name"), max_length=128, blank=True)

    # Styling settings...

    logo = AnyImageField(verbose_name=_('logo'), upload_to='logos', blank=True)
    bg_color = models.CharField(_("background color"), max_length=32, blank=True,
        help_text=_("""Either color name or HEX value, for example: "red" or "#FF0000" """))
    fg_color = models.CharField(_("foreground color"), max_length=32, blank=True,
        help_text=_("""Either color name or HEX value, for example: "red" or "#FF0000" """))

    # Google settings...

    keycode = models.CharField(
        _("Google Analytics UID"), max_length=80, blank=True,
        help_text=_("The User ID of the google analytics account to use on "
                    "the site. e.g. UA-1234567-1")
    )
    fs = FileSystemStorage(
        location=os.path.join(os.path.join(settings.SETTINGS_DIR, 'google'))
    )
    client_secrets = models.FileField(storage=fs,
        verbose_name=_('Client secrets'), upload_to='', blank=True,
        help_text=_("The JSON credentials file for a 'service account key', "
                    "which can be obtained from the Google API Console.")
        )
    analytics_profile_id = models.CharField(
        verbose_name=_("Google Analytics profile ID"), max_length=24,
        blank=True, help_text=_("The ID of the Google Analytics profile to "
                                "display stats for on the dashboard.")
    )
    maps_api_key = models.CharField(
        verbose_name=_("Google Maps JS API key"), max_length=80,
        blank=True, help_text=_("The API key to use for display of google "
                                "maps.")
    )

    class Meta:
        abstract = True
