# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings
import any_imagefield.models.fields


class Migration(migrations.Migration):

    dependencies = [
        ('sites', '0001_initial'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='SiteSettings',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created', models.DateTimeField(auto_now_add=True, verbose_name='Created at')),
                ('modified', models.DateTimeField(auto_now=True, verbose_name='Last Updated')),
                ('site_name', models.CharField(max_length=100, verbose_name='Site Name')),
                ('contact_email', models.CharField(max_length=128, verbose_name='Contact Email')),
                ('favicon', any_imagefield.models.fields.AnyImageField(upload_to=b'favicons', verbose_name='favicon', blank=True)),
                ('keycode', models.CharField(max_length=80, verbose_name='Google Analytics Keycode', blank=True)),
                ('send_mail', models.BooleanField(default=False, help_text='Enable / disable sending of emails by the site.', verbose_name='Send Emails?')),
                ('site', models.OneToOneField(related_name='usersettings', null=True, editable=False, to='sites.Site')),
                ('user', models.ForeignKey(related_name='usersettings', editable=False, to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'abstract': False,
            },
            bases=(models.Model,),
        ),
    ]
