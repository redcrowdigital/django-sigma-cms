# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import any_imagefield.models.fields


class Migration(migrations.Migration):

    dependencies = [
        ('sitesettings', '0003_sitesettings_backup_frequency'),
    ]

    operations = [
        migrations.AddField(
            model_name='sitesettings',
            name='bg_color',
            field=models.CharField(help_text='Either color name or HEX value, for example: "red" or "#FF0000" ', max_length=32, blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='sitesettings',
            name='fg_color',
            field=models.CharField(help_text='Either color name or HEX value, for example: "red" or "#FF0000" ', max_length=32, blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='sitesettings',
            name='logo',
            field=any_imagefield.models.fields.AnyImageField(upload_to=b'logos', verbose_name='logo', blank=True),
            preserve_default=True,
        ),
    ]
