# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('sitesettings', '0004_auto_20160714_1042'),
    ]

    operations = [
        migrations.AddField(
            model_name='sitesettings',
            name='analytics_profile_id',
            field=models.CharField(help_text='The ID of the Google Analytics profile to display stats for on the dashboard.', max_length=24, verbose_name='Google Analytics profile ID', blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='sitesettings',
            name='client_secrets',
            field=models.FileField(help_text="The JSON credentials file for a 'service account key', which can be obtained from the Google API Console.", upload_to=b'settings', verbose_name='Client secrets', blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='sitesettings',
            name='bg_color',
            field=models.CharField(help_text='Either color name or HEX value, for example: "red" or "#FF0000" ', max_length=32, verbose_name='background color', blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='sitesettings',
            name='fg_color',
            field=models.CharField(help_text='Either color name or HEX value, for example: "red" or "#FF0000" ', max_length=32, verbose_name='foreground color', blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='sitesettings',
            name='keycode',
            field=models.CharField(help_text='The User ID of the google analytics account to use on the site. e.g. UA-1234567-1', max_length=80, verbose_name='Google Analytics UID', blank=True),
            preserve_default=True,
        ),
    ]
