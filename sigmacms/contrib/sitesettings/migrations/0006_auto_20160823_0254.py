# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.core.files.storage


class Migration(migrations.Migration):

    dependencies = [
        ('sitesettings', '0005_auto_20160817_1202'),
    ]

    operations = [
        migrations.AlterField(
            model_name='sitesettings',
            name='client_secrets',
            field=models.FileField(help_text="The JSON credentials file for a 'service account key', which can be obtained from the Google API Console.", upload_to=b'', storage=django.core.files.storage.FileSystemStorage(location=b'/Users/simon/Sites/musicaviva/djangoapp/musicaviva/settings/google'), verbose_name='Client secrets', blank=True),
            preserve_default=True,
        ),
    ]
