# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('sitesettings', '0006_auto_20160823_0254'),
    ]

    operations = [
        migrations.AddField(
            model_name='sitesettings',
            name='maps_api_key',
            field=models.CharField(help_text='The API key to use for display of google maps.', max_length=80, verbose_name='Google Maps JS API key', blank=True),
            preserve_default=True,
        ),
    ]
