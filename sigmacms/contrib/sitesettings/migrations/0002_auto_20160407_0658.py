# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('sitesettings', '0001_initial'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='sitesettings',
            options={'verbose_name': 'Settings', 'verbose_name_plural': 'Settings'},
        ),
        migrations.AddField(
            model_name='sitesettings',
            name='access_key',
            field=models.CharField(max_length=128, verbose_name='Amazon S3 Access Key', blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='sitesettings',
            name='bucket_name',
            field=models.CharField(max_length=128, verbose_name='Amazon S3 Bucket Name', blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='sitesettings',
            name='dbbackup_enabled',
            field=models.BooleanField(default=False, help_text='Enable / disable database backups.', verbose_name='Backup Database?'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='sitesettings',
            name='mediabackup_enabled',
            field=models.BooleanField(default=False, help_text='Enable / disable media files backups.', verbose_name='Backup Media Files?'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='sitesettings',
            name='secret_key',
            field=models.CharField(max_length=128, verbose_name='Amazon S3 Secret Key', blank=True),
            preserve_default=True,
        ),
    ]
