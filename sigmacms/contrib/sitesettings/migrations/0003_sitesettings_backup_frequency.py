# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import djchoices.choices


class Migration(migrations.Migration):

    dependencies = [
        ('sitesettings', '0002_auto_20160407_0658'),
    ]

    operations = [
        migrations.AddField(
            model_name='sitesettings',
            name='backup_frequency',
            field=models.CharField(default=b'd', max_length=1, choices=[(b'd', b'daily'), (b'w', b'weekly'), (b'm', b'monthly')], verbose_name='Backup Frequency', validators=[djchoices.choices.ChoicesValidator({b'm': b'monthly', b'd': b'daily', b'w': b'weekly'})]),
            preserve_default=True,
        ),
    ]
