# -*- coding: utf-8 -*-

from django import forms

from .models import AbstractFile


class FilePickerForm(forms.ModelForm):

    class Meta:
        model = AbstractFile
        fields = ['file']
