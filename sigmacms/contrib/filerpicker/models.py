# -*- coding: utf-8 -*-

from django.db import models
from django.utils.translation import ugettext as _

from filer.fields.file import FilerFileField


class AbstractFile(models.Model):

    file = FilerFileField(verbose_name=_("File"))

    class Meta:
        abstract = True

