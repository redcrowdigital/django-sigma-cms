# -*- coding: utf-8 -*-

import warnings

from django import forms
from django.contrib import admin
from django.http import HttpResponse
from django.contrib.admin import helpers
from django.views.generic import FormView
from django.utils.translation import ugettext as _
from django.contrib.admin.templatetags.admin_static import static
from django.conf import settings

from filer.models import File

from .forms import FilePickerForm


@admin.site.register_view('filerpicker/', urlname='filerpicker', visible=False)
class FilerPicker(FormView):
    template_name = 'filerpicker/admin/filerpicker.html'
    form_class = FilePickerForm
    success_url = '../'
    fieldsets = None

    fields = ['file']
    
    @property
    def media(self):
        extra = '' if settings.DEBUG else '.min'
        js = [
            'core.js',
            'admin/RelatedObjectLookups.js',
            'jquery%s.js' % extra,
            'jquery.init.js'
        ]
        return forms.Media(js=[static('admin/js/%s' % url) for url in js])

    def get_fields(self, request, obj=None):
        return self.fields

    def get_fieldsets(self, request, obj=None):
        if self.fieldsets:
            return self.fieldsets
        return [(None, {'fields': self.get_fields(request, obj)})]

    def get_context_data(self, **kwargs):
        from django.contrib.admin.options import IS_POPUP_VAR

        form = kwargs.pop('form')
        adminForm = helpers.AdminForm(
            form, list(self.get_fieldsets(self.request)), {}, None, model_admin=None)
        media = self.media + adminForm.media

        context = {
            'change': True,
            'title': _('Filer Picker'),
            'adminform': adminForm,
            'has_file_field': True,
            'app_label': 'filerpicker',
            'opts': dict(app_label='filerpicker', model_name='image'),
            'media': media,
            'errors': helpers.AdminErrorList(form, []),
            'is_popup': (IS_POPUP_VAR in self.request.POST or
                         IS_POPUP_VAR in self.request.GET),
        }
        context.update(kwargs)
        return super(FormView, self).get_context_data(**context)

    def form_valid(self, form):
        if self.request.is_ajax():
            return HttpResponse(form.cleaned_data["file"].url, content_type="text/plain")
        return super(FormView, self).form_valid(form)
