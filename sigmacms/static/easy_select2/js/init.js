/**
 * Created by user on 30.06.16.
 */
(function ($) {
    "use strict";
    function redisplay_select2($el){
        $("#" + $el.attr('id')).on('select2changed', function(e) {
            $($(this)).select2($el.data());

            var relatedWidgetCSSSelector = '.related-widget-wrapper-change-link, .related-widget-wrapper-delete-link',
                hrefTemplateAttr = 'data-href-template';

            $('#container').delegate('.related-widget-wrapper', 'change', function(event){
                var siblings = $(this).nextAll(relatedWidgetCSSSelector),
                    value = event.target.value;
                if (!siblings.length) return;
                if (value) {
                   siblings.each(function(){
                      var elm = $(this);
                      elm.attr('href', elm.attr(hrefTemplateAttr).replace('__pk__', value));
                   });
                } else siblings.removeAttr('href');
            });

        }).trigger('select2changed');

        $el.remove();
    }
    function add_select2_handlers() {
        $('div.field-easy-select2:not([id*="__prefix__"])').each(function () {
            redisplay_select2($(this));
        });
    }
    $(function () {
        add_select2_handlers();
    });

    $(document).bind('DOMNodeInserted', function(e) {
        $(e.target).find('div.field-easy-select2:not([id*="__prefix__"])').each(function () {
            redisplay_select2($(this));
        });
    });
}(jQuery || django.jQuery));
