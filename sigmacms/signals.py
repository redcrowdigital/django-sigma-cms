def publisher_pre_delete(sender, instance, **kwargs):
    if not instance:
        return

    # If the draft record is deleted, the published object should be as well
    if instance.is_draft and instance.publisher_linked:
        instance.publisher_linked.delete()


