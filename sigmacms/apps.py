# -*- coding: utf-8 -*-

from django.conf import settings
from django.apps import AppConfig, apps
from django.db.models.signals import post_init


def handle_user_model(sender, instance, **kwargs):
    from django.contrib.auth import get_user_model
    from django.contrib.auth.models import _user_has_perm, _user_has_module_perms

    USERNAME_FIELD = get_user_model().USERNAME_FIELD

    sender.add_to_class('username', lambda u: u.email)

    sender.add_to_class('__unicode__', lambda u: u.email or getattr(u, USERNAME_FIELD))

    def has_perm(cls, perm, obj=None):
        # Active superusers have all permissions.
        if cls.is_active and cls.is_superuser:
            return True

        if cls.is_active and cls.is_editor:
            if 'fluent_pages' in settings.INSTALLED_APPS:
                from fluent_pages.extensions import page_type_pool
                from fluent_contents.extensions import plugin_pool

                for m in ['fluent_pages', 'fluent_contents', 'fluentpage', 'sharedcontent']:
                    if m in perm:
                        return True

                for m in [c._meta.model_name for c in page_type_pool.get_model_classes()]:
                    if m in perm:
                        return True

                for m in [c._meta.model_name for c in plugin_pool.get_model_classes()]:
                    if m in perm:
                        return True

            if 'filer' in settings.INSTALLED_APPS:
                for m in ['filer']:
                    if m in perm:
                        return True

            if 'fluentcms_forms_builder' in settings.INSTALLED_APPS:
                for m in ['fluentcms_forms_builder']:
                    if m in perm:
                        return True

            if 'sigmacms' in settings.INSTALLED_APPS:
                for m in ['slideshow']:
                    if m in perm:
                        return True

        # Otherwise we need to check the backends.
        return _user_has_perm(cls, perm, obj)

    sender.add_to_class('has_perm', has_perm)

    def has_module_perms(cls, app_label):
        # Active superusers have all permissions.
        if cls.is_active and cls.is_superuser:
            return True

        if cls.is_active and cls.is_editor:
            if 'fluent_pages' in settings.INSTALLED_APPS:
                from fluent_pages.extensions import page_type_pool
                from fluent_contents.extensions import plugin_pool

                if app_label in ['fluent_pages', 'fluent_contents', 'fluentpage', 'sharedcontent']:
                    return True

                if app_label in [c._meta.model_name for c in page_type_pool.get_model_classes()]:
                    return True

                if app_label in [c._meta.model_name for c in plugin_pool.get_model_classes()]:
                    return True

            if 'filer' in settings.INSTALLED_APPS:
                if app_label in ['filer']:
                    return True

            if 'fluentcms_forms_builder' in settings.INSTALLED_APPS:
                if app_label in ['fluentcms_forms_builder']:
                    return True

            if 'sigmacms' in settings.INSTALLED_APPS:
                if app_label in ['slideshow']:
                    return True

        # Otherwise we need to check the backends.
        return _user_has_module_perms(cls, app_label)

    sender.add_to_class('has_module_perms', has_module_perms)

    def get_editor_status(cls):
        if cls.is_superuser is True:
            return True

        elif cls.is_staff is True:
            if "Publishers" in cls.groups.values_list('name', flat=True):
                return True

            elif "Editors" in cls.groups.values_list('name', flat=True):
                return True

        return False

    def set_editor_status(cls, value):
        from django.contrib.auth.models import Group

        if not isinstance(value, bool):
            raise ValueError("Boolean value must be True or False")

        group, created = Group.objects.get_or_create(name="Editors")
        if value is True:
            if cls.is_staff is False:
                cls.is_staff = True
            cls.groups.add(group)
        else:
            cls.groups.remove(group)

    sender.add_to_class('is_editor', property(get_editor_status, set_editor_status))

    def get_publisher_status(cls):
        if cls.is_superuser is True:
            return True

        elif cls.is_staff is True:
            if "Publishers" in cls.groups.values_list('name', flat=True):
                return True

        return False

    def set_publisher_status(cls, value):
        from django.contrib.auth.models import Group

        if not isinstance(value, bool):
            raise ValueError("Boolean value must be True or False")

        group, created = Group.objects.get_or_create(name="Publishers")
        if value is True:
            if cls.is_staff is False:
                cls.is_staff = True
            cls.groups.add(group)
        else:
            cls.groups.remove(group)

    sender.add_to_class('is_publisher', property(get_publisher_status, set_publisher_status))


class SigmaCMSConfig(AppConfig):
    label = name = 'sigmacms'

    def ready(self):
        try:
            from django.contrib.auth import get_user_model
            from django.contrib.auth.models import Group

            # make sure all required groups are exists
            for name in ("Editors", "Publishers"):
                Group.objects.get_or_create(name=name)

            post_init.connect(handle_user_model, sender=get_user_model())

            if 'fluent_pages' in settings.INSTALLED_APPS:
                from fluent_pages.extensions import page_type_pool
                from fluent_contents.extensions import plugin_pool

                from .admin.mixins import StaffAdminMixin

                if 'publisher' in settings.INSTALLED_APPS:
                    from fluent_pages.models import UrlNode, Page, HtmlPage
                    from .managers import PublisherManager, PublisherQuerySet, UrlNodeManager

                    for klass in (UrlNode, Page, HtmlPage):
                        # replace default managers...
                        klass.add_to_class('_default_manager', UrlNodeManager())
                        klass.add_to_class('objects', UrlNodeManager())

                        # add custom ``publisher`` manager...
                        klass.add_to_class('publisher_manager', PublisherManager.for_queryset_class(
                            PublisherQuerySet)())

                for p in page_type_pool.get_plugins():
                    class PageTypeAdmin(StaffAdminMixin, p.model_admin):
                        pass
                    p.model_admin = PageTypeAdmin

                for p in plugin_pool.get_plugins():
                    try:
                        class PluginAdmin(StaffAdminMixin, p.model_admin):
                            pass

                        p.model_admin = PluginAdmin
                    except AttributeError:
                        pass

            # FIX: ugly monkey patching...
            # TODO: replace this crap with configurable option, for example:
            # FLUENT_CONTENTS_CONTEXT_PROCESSORS = (
            #     ...
            # )
            if 'responsive' in settings.INSTALLED_APPS:
                import django.contrib.auth.context_processors
                import django.contrib.messages.context_processors
                try:
                    from django.template import context_processors  # Django 1.8+
                except ImportError:
                    from django.core import context_processors

                import responsive.context_processors

                def _add_debug(request):
                    return {'debug': settings.DEBUG}

                _STANDARD_REQUEST_CONTEXT_PROCESSORS = (
                    context_processors.csrf,
                    context_processors.debug,
                    context_processors.i18n,
                    context_processors.media,
                    context_processors.request,
                    context_processors.static,
                    django.contrib.auth.context_processors.auth,
                    django.contrib.messages.context_processors.messages,
                    _add_debug,
                    # added ``responsive`` context processors
                    responsive.context_processors.device,
                )

                from fluent_contents.extensions import pluginbase

                pluginbase._STANDARD_REQUEST_CONTEXT_PROCESSORS = _STANDARD_REQUEST_CONTEXT_PROCESSORS

        except:
            pass  # shit happens
