VERSION = (0, 4, 6, 6)

__version__ = '.'.join([str(n) for n in VERSION])

default_app_config = 'sigmacms.apps.SigmaCMSConfig'
