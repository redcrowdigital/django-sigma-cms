import os
from setuptools import setup, find_packages

README = open(os.path.join(os.path.dirname(__file__), 'README.md')).read()

# allow setup.py to be run from any path
os.chdir(os.path.normpath(os.path.join(os.path.abspath(__file__), os.pardir)))

from sigmacms import __version__

setup(
    name='django-sigmacms',
    version=__version__,
    packages=find_packages(exclude=['example']),
    include_package_data=True,
    license='Proprietary License',
    description='Put short description here...',
    long_description=README,
    url='https://bitbucket.org/sjit/django-sigma-cms',
    author='Basil Shubin',
    author_email='basil.shubin@gmail.com',
    dependency_links = [
        "https://github.com/sigmacms/django-polymorphic-tree/archive/sigmacms.zip#egg=django-polymorphic-tree-1.2.6",
        "https://github.com/sigmacms/django-fluent-pages/archive/sigmacms.zip#egg=sigmacms-fluent-pages-0.9.3",
    ],
    install_requires=[
        'django==1.7.11',

        'pytz==2016.3',
        'micawber==0.3.3',
        'easy-thumbnails==2.2',
        'html5lib==0.999',
        'bleach==1.4.1',

        'django-polymorphic==0.7.1',

        'django-any-urlfield==2.1.1',
        'django-any-imagefield==0.8.2',

        'django-parler==1.5',
        'django-fluent-utils==1.2.1',
        'django-fluent-contents==1.1.5',

        'django-mptt==0.7.4',
        'django-polymorphic-tree>1.2.5,<1.3',
        'sigmacms-fluent-pages==0.9.3',

        'django-filer==0.9.11',

        'django-model-utils==2.0.3',
        'django-model-publisher==0.1.4',

        'django-easy-maps==0.9.2',
        'django-wysiwyg==0.7.1',

        'django-axes==2.3.0',
        'django-loginas==0.1.9',
        'django-logentry-admin==0.1.5',
        'boto==2.39.0',
        'django-storages-redux==1.3.2',

        'django-etc==0.9.1',
        'django-choices==1.4.2',
        'django-dbbackup==2.5.0',
        'django-easy-select2==1.3.3',
        'django-admin-enhancer==1.0',
        'django-usersettings2==0.1.5',
        'django-adminplus==0.4',

        'django-taggit==0.21.6',
    ],
    classifiers = [
        'Environment :: Web Environment',
        'Framework :: Django',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: Proprietary License',
        'Operating System :: OS Independent',
        'Programming Language :: Python',
        'Programming Language :: Python :: 2',
        'Programming Language :: Python :: 2.7',
        'Topic :: Internet :: WWW/HTTP',
        'Topic :: Internet :: WWW/HTTP :: Dynamic Content',
    ],
    zip_safe=False,
)
