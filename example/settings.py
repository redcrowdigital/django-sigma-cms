"""
Django settings for example project.

For more information on this file, see
https://docs.djangoproject.com/en/1.7/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.7/ref/settings/
"""

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
import os
BASE_DIR = os.path.dirname(os.path.dirname(__file__))

SETTINGS_DIR = os.path.join(os.path.join(BASE_DIR, 'settings'))

from django.utils.translation import ugettext_lazy as _


# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.7/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = 'YOUR_SECRET_KEY'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True
TEMPLATE_DEBUG = True

SERVE_MEDIA = True

ALLOWED_HOSTS = []

INTERNAL_IPS = ['127.0.0.1']

SITE_DOMAIN = 'example.com'
SITE_ID = 1


# Application definition

DJANGO_APPS = [
    'django.contrib.auth',
    'django.contrib.sessions',
    'django.contrib.contenttypes',
    'django.contrib.staticfiles',
    'django.contrib.messages',
    'django.contrib.sites',
]

EXTERNAL_APPS = [
    'object_tools',

    'suit',
    'responsive',

    'export',

    'taggit',
    'socialaggregator',

    'el_pagination',

    'fluentcms_forms_builder',

    'dbbackup',
    'django_cron',
    'django_extensions',
    'post_office',
    'authtools',
    'haystack',
]

PROJECT_APPS = [
    'sigmacms',
    'sigmacms.pagetypes.redirectnode',
    'sigmacms.pagetypes.simplepage',
    'sigmacms.pagetypes.listnode',

    'sigmacms.plugins.file',
    'sigmacms.plugins.picture',
    'sigmacms.plugins.googlemap',
    'sigmacms.plugins.socialfeed',
    'sigmacms.plugins.twitterfeed',
    'sigmacms.plugins.oembeditem',
    'sigmacms.plugins.rawhtml',
    'sigmacms.plugins.teaser',
    'sigmacms.plugins.slideshow',
]

INSTALLED_APPS = DJANGO_APPS + EXTERNAL_APPS + PROJECT_APPS

if 'fluentcms_forms_builder' in INSTALLED_APPS:
    INSTALLED_APPS += [
        'forms_builder.forms',
    ]

MIDDLEWARE_CLASSES = [
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.auth.middleware.SessionAuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    # 'django.middleware.security.SecurityMiddleware',
]

if 'responsive' in INSTALLED_APPS:
   MIDDLEWARE_CLASSES += [
       'responsive.middleware.ResponsiveMiddleware',
   ]

TEMPLATE_DIRS = (
    os.path.join(os.path.dirname(__file__), 'templates'),
    os.path.join(os.path.dirname(__file__), 'templates', 'layouts'),
)

TEMPLATE_LOADERS = (
    ('django.template.loaders.cached.Loader', (
        'django.template.loaders.filesystem.Loader',
        'django.template.loaders.app_directories.Loader',
    )),
)

TEMPLATE_CONTEXT_PROCESSORS = [
    'django.core.context_processors.tz',
    'django.core.context_processors.i18n',
    'django.core.context_processors.debug',
    'django.core.context_processors.static',
    'django.core.context_processors.request',
    'django.contrib.auth.context_processors.auth',
    'django.contrib.messages.context_processors.messages',

    'sigmacms.context_processors.usersettings',
]

if 'responsive' in INSTALLED_APPS:
   TEMPLATE_CONTEXT_PROCESSORS += [
       'responsive.context_processors.device',
   ]

ROOT_URLCONF = 'example.urls'


# Database
# https://docs.djangoproject.com/en/1.7/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
    }
}

CACHES = {
    'default': {
        'BACKEND': 'django.core.cache.backends.locmem.LocMemCache',
        'LOCATION': 'unique-snowflake',
    },
}


# Internationalization
# https://docs.djangoproject.com/en/1.7/topics/i18n/

LANGUAGE_CODE = 'en'
LANGUAGES = (
    ('en', _('English')),
    ('es', _('Spanish')),
    ('fr', _('French')),
)

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.8/howto/static-files/

# Absolute filesystem path to the directory that will hold user-uploaded files.
MEDIA_ROOT = os.path.join(os.path.dirname(__file__), 'media')

# URL that handles the media served from MEDIA_ROOT. Make sure to use a
# trailing slash.
MEDIA_URL = '/media/'

# Absolute path to the directory static files should be collected to.
STATIC_ROOT = os.path.join(os.path.dirname(__file__), 'static')

STATIC_URL = '/static/'


## Email / notification settings

if 'post_office' in INSTALLED_APPS:
    EMAIL_BACKEND = 'post_office.EmailBackend'


## Auth settings

if 'authtools' in INSTALLED_APPS:
    AUTH_USER_MODEL = 'authtools.User'

AUTHENTICATION_BACKENDS = [
    'authtools.backends.CaseInsensitiveEmailModelBackend',
]


## Haystack settings

if 'haystack' in INSTALLED_APPS:
    HAYSTACK_CONNECTIONS = {
        'default': {
            'ENGINE': 'haystack.backends.whoosh_backend.WhooshEngine',
            'PATH': os.path.join(BASE_DIR, 'whoosh_index', ),
        },
    }
    for lang in LANGUAGES:
        HAYSTACK_CONNECTIONS.update({
            lang[0]: {
                'ENGINE': 'haystack.backends.whoosh_backend.WhooshEngine',
                'PATH': os.path.join(BASE_DIR, 'whoosh_index', lang[0]),
        }
    })


## Django Suit settings

SUIT_CONFIG = {
    'ADMIN_NAME': 'Sigma CMS',
    'SEARCH_URL': '',
    'CONFIRM_UNSAVED_CHANGES': False,
    'MENU_ICONS': {
        'fluent_pages': 'icon-file',
        'filer': 'icon-picture',
    },

    'MENU': (
        {'app': 'fluent_pages', 'label': 'CMS', 'icon':'icon-file', 'models': (
            'fluent_pages.page',
            'fluent_pages.pagelayout',
            'sharedcontent.sharedcontent',
            'sitesettings.sitesettings',
        )},
        {'app': 'filer', 'label': 'Media Library', 'icon':'icon-picture'},
    ),
}


## Django-responsive2 settings

RESPONSIVE_MEDIA_QUERIES = {
    'small': {
        'verbose_name': _('Small screens'),
        'min_width': None,
        'max_width': 640,
    },
    'medium': {
        'verbose_name': _('Medium screens'),
        'min_width': 641,
        'max_width': 1023,
    },
    'large': {
        'verbose_name': _('Large screens'),
        'min_width': 1024,
        'max_width': 1440,
    },
    'xlarge': {
        'verbose_name': _('XLarge screens'),
        'min_width': 1441,
        'max_width': 1920,
    },
    'xxlarge': {
        'verbose_name': _('XXLarge screens'),
        'min_width': 1921,
        'max_width': None,
    }
}


## Social aggregator settings

from socialaggregator.settings import *

EDSA_TWITTER_CONSUMER_KEY = "pu4UtuctVK8u6yOIu81ixwxOX"
EDSA_TWITTER_CONSUMER_SECRET = "5XDqP3zDvoqk8Bb0hRM0UDnhMCWDz09eLfNeEqoyR02SOAFwcE"
EDSA_TWITTER_TOKEN = "2175379332-wieB9lFsJabN00ZfAteviMVe4TDhb2dbgst5e7L"
EDSA_TWITTER_SECRET = "wyCW7cTxqOJpeKk7Rpb9FmtUEFpSO8cv3fbq3BjaaqyWE"

EDSA_FB_APP_ID = "242446442761193"
EDSA_FB_APP_SECRET = "aa70da982c4377461c84a8d5201d53da"

EDSA_INSTAGRAM_ACCESS_TOKEN = '3054158068.1677ed0.ddedfc5b1578423ea6913f4299cb7380'

EDSA_LINKEDIN_ACCESS_TOKEN = 'AQX9uLvYVf4Eun-vVWemPczjKYY96KkrLmoLYojYn0EAB_4JrMeSFrZFqtzu938FwUKo5z_83JpKNA89-7H-a7hZjN4lEs7lA5Kvy74a_LfmA5REpFNp27oCZmp52wU9ufmQzabr7zCLG1qFf3ELuwTBpQW9zBlVA-8cf3dDX5FQxYflCaI'

EDSA_PLUGINS = {
    "edsa_twitter": {
        "ENGINE": "socialaggregator.plugins.twitter_noretweet_aggregator",
        "NAME": "Twitter"
    },
    "edsa_instagram": {
        "ENGINE": "socialaggregator.plugins.instagram_aggregator",
        "NAME": "Instagram"
    },
    "edsa_facebook_fanpage": {
        "ENGINE": "socialaggregator.plugins.facebook_fanpage_aggregator",
        "NAME": "Facebook Fanpage"
    },
    "edsa_linkedin": {
        "ENGINE": "socialaggregator.plugins.linkedin_company_aggregator",
        "NAME": "LinkedIn Company"
    },
}


## django-analytics-tools settings

GOOGLE_OAUTH_CREDENTIALS = os.path.join(SETTINGS_DIR, 'client_secrets.json')
GOOGLE_ANALYTICS_PROFILE = ''


## Sigma CMS settings

FLUENT_PAGES_TEMPLATE_DIR = os.path.join(
    os.path.dirname(__file__), 'templates', 'layouts')
FLUENT_PAGES_DEFAULT_PAGETYPE = 'simplepage.SimplePage'

from sigmacms.config import setup
setup(conf=locals())
